#!/bin/bash
#
# This script prepares freshly installed MTCA host
# 
# Author	: Davit Kalantaryan (Email: davit.kalantaryan@desy.de)
# Created	: 2015 Jul 15
# Documentation	: https://www-zeuthen.desy.de/~kalantar/public/prepare/prepare.html
# 		   If there is a link to above mentioned document, then it will be like this (p. x), where x 
# 		   is the part pf the document 
# Last Modified	: 2017 Jun 9
#


# check if user is root
USER1=`id -nu`
if [ "${USER1}" != "root" ] ; then
	echo "!!! only root can start this script !!!"
	exit 2
fi

#  1.		First repository list should be updated and prepared!
		  apt-get update

#  2.		Good GUI text editor makes life easier
		  apt-get install nedit

#  3. (p. 10).	Installation of linux kernel sources (usually necessary)
#		For further version of this script this point will be confirmed after asking question
		  apt-get install linux-source

#  4. (p. 11)	Installation of linux headers if previous step is not done
		  apt-get install linux-headers-$(uname -r)

#  5. (p. 12)	Install NTP client
		  apt-get install ntp
		  #sudo nedit /etc/ntp.conf # this file should be modified to set correct time server
		  PERL_PATH=`which perl`
		  if [ -z "$PERL_PATH" ]; then
			apt-get install perl
		  fi
		  perl -pi -e 's/ntp.ubuntu.com/timesrv.ifh.de/g' /etc/ntp.conf
		  /etc/init.d/ntp restart

#  6. (p. 13)	Installing afs_client+kerberos
		  apt-get install openafs-krb5 openafs-client krb5-user module-assistant openafs-modules-dkms
		  dpkg-reconfigure krb5-config openafs-client
		  m-a prepare
		  m-a auto-install openafs
		  modprobe openafs
		  service openafs-client restart

#  7.		Create all directories on /export
		  mkdir /export/doocs
		  mkdir /export/home

#  8.		Create symbolic linc doocs
		  ln -s /afs/ifh.de/group/pitz/doocs /doocs
		  
#  9.		Add user for Hamburg experts
		  groupadd -g 216 pitz
		  useradd -c "Hamburg Expert" --home /export/home/hamburgexpert --gid 216 -m --shell /bin/bash --uid 1001 hamburgexpert
		  passwd hamburgexpert

# 10. (p. 06)	Change ssh to kerberos
		  cp /doocs/amd64_linux26/pam.d/sshd  /etc/pam.d/.
		  apt-get install libpam-afs-session
		  cp /doocs/amd64_linux26/pam.d/password-auth /etc/pam.d/.

# 11. (p. 14)	Install portmapper service
		  apt-get install rpcbind

# 12. (p. 15)	Change portmaper service to insecure mod
		  echo 'OPTIONS="-w -i"' | tee /etc/default/rpcbind
		  #service portmap restart
		  service rpcbind restart

# 13. (p. 19)	Modify /etc/environment
		  ####echo "LD_LIBRARY_PATH=/doocs/lib:/doocs/bin/qtbin:$LD_LIBRARY_PATH" >> /etc/environment
		  ##echo "PATH=/doocs/bin:$PATH" >> /etc/environment
		  #echo "PATH=/export/doocs/bin:$PATH" >> /etc/environment  # how to do nt
		  ####echo "LD_LIBRARY_PATH=/doocs/lib" >> /etc/environment
		  echo "ENSHOST=picus7:picus8" >> /etc/environment

# 14.		Add new conf file for /etc/ld.so
		  touch /etc/ld.so.conf.d/desylibs.conf	
		  echo "# Paths for libraries prepared at DESY" >> /etc/ld.so.conf.d/desylibs.conf
		  echo "/export/doocs/lib" >> /etc/ld.so.conf.d/desylibs.conf
		  echo "/local/lib" >> /etc/ld.so.conf.d/desylibs.conf
		  ldconfig
		  

# 15. (p. 17)	Hotplugging should be activated
		  echo "Activate hot plugging manually if it is needed!"

# 16. (p. 18)	SOL should be activated
		  echo "Activate grub and LINUX SOL manually if hardware exists and needed!"

# 17.		All necessary users should be added, currently it is done manually, 
#		in alphabetical order. Later on should be done, using script
		  /doocs/bin/local_create_mtca_znaccount_prvt bagrat
		  /doocs/bin/local_create_mtca_znaccount_prvt gut
		  /doocs/bin/local_create_mtca_znaccount_prvt kalantar
		  /doocs/bin/local_create_mtca_znaccount_prvt mdavid
		  /doocs/bin/local_create_mtca_znaccount_prvt sweisse
		  /doocs/bin/local_create_mtca_znaccount_prvt tonisch
		  useradd -c "Doocs Admin" --home /export/home/doocsadm --gid 216 -m --shell /bin/bash --uid 995 doocsadm

# 18.		Change owner of directory /export/doocs
		  chown doocsadm:pitz /export/doocs

# 19.		Configure repositories from Hamburg
		  wget -O - http://doocs.desy.de/pub/doocs/DESY-Debian-key.asc | apt-key add -
		  echo "deb http://doocs.desy.de/pub/doocs `lsb_release -sc` main" > /etc/apt/sources.list.d/doocs.list
		  apt-get update

# 20.		Install watchdog server from Hamburg repository
		  apt-get install doocs-watchdog-server
		  # other repositories from Hamburg

# 21.		Install qt, for qthardmon and friends
		  #sudo apt-get install qt5-default

