
// 2017 Jul 04

//#ifndef pitz_adc_remoteplotspectrum_centralwidget_cpp

#include "pitz_adc_remoteplotspectrum_centralwidget.hpp"

pitz::adc::remoteplotspectrum::CentralWidget::CentralWidget()
{
    m_mainLayout.addWidget(&m_graphicWidget);
    setLayout(&m_mainLayout);
}


pitz::adc::remoteplotspectrum::CentralWidget::~CentralWidget()
{
    m_mainLayout.removeWidget(&m_graphicWidget);
}


common::ui::qt::GraphicWidget& pitz::adc::remoteplotspectrum::CentralWidget::GraphWidget()
{
    return m_graphicWidget;
}

