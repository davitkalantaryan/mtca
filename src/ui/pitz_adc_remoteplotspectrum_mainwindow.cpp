
// 2017 Jul 04


#include "pitz_adc_remoteplotspectrum_mainwindow.hpp"
#include <zmq.h>
#include <QMenuBar>


pitz::adc::remoteplotspectrum::MainWindow::MainWindow()
    :
      m_actionConnect(QIcon(":/img/connect_btn.png"),tr("Connect"),this),
      m_actionConnectinDetails("Connection details",this),
      m_subscriber(1000)

{
    QMenu* pCurMenu ;
    QMenuBar* pMenuBar = menuBar();

    m_nBoardNum=12;m_nChanNum=0;
    m_hostName = "mtcapitzcpu3";

    m_bConnected = false;

    m_toolBar.addAction(&m_actionConnect);
    setCentralWidget(&m_centrawWidget);

    m_actionConnectinDetails.setStatusTip( tr("Details on the publisher pear") );
    connect( &m_actionConnectinDetails, SIGNAL(triggered()), this, SLOT(ActionConnectDetailsSlot()) );

    pCurMenu = pMenuBar->addMenu( tr("&Connection") );
    pCurMenu->addAction(&m_actionConnectinDetails);
    pCurMenu->addAction(&m_actionConnect);

    connect( &m_actionConnect, SIGNAL(triggered()), this, SLOT(ActionConnectDisconnectSlot()) );
}



pitz::adc::remoteplotspectrum::MainWindow::~MainWindow()
{
    StopSubscriberThread();
}


#include <sys/timeb.h>
#include <time.h>
int g_nDebug;


void pitz::adc::remoteplotspectrum::MainWindow::SubscriberThreadFunc()
{
    int nReceived;
    bool bIsInstintatnt(true);
    timeb timeOld, timeNew;
    long int lnTimeDiff;
    int nBufferIndex(0), nDiff;


    ftime(&timeOld);

    try
    {
        adc_data_serializer aSerializer2;
        adc_data_serializer* vpSerializers[2];

        vpSerializers[0] = &m_serializer;
        vpSerializers[1] = &aSerializer2;

        nReceived = m_subscriber.recv_data(
                    aSerializer2.get_buffer(),adc_data_serializer::header_size);

        if(nReceived<adc_data_serializer::header_size){
            return;
        }

        aSerializer2.prepare_buffer_based_on_header();

        while(m_bConnected){
            nReceived = m_subscriber.recv_data(
                        vpSerializers[nBufferIndex]->get_buffer(),m_serializer.get_buffer_size());

            nDiff = memcmp(aSerializer2.get_spectrum_array<float>(),
                           m_serializer.get_spectrum_array<float>(),
                           m_serializer.get_spectrum_elements_number()*4);


            ftime(&timeNew);

            lnTimeDiff = (long int)((timeNew.time-timeOld.time)*1000) +
                    ( ((long int)timeNew.millitm)-((long int)timeOld.millitm)  );
            timeOld = timeNew;

            if(g_nDebug){
                printf("lnTimeDiff = %ld, bufDiff=%d, nBufferIndex=%d\n",lnTimeDiff,nDiff,nBufferIndex);
            }

            if(nReceived>0){
                m_serializer.prepare_spectrum<float>();
                m_centrawWidget.GraphWidget().SetSpectrum<float>(
                            vpSerializers[nBufferIndex++]->get_spectrum_array<float>(),
                            m_serializer.get_spectrum_elements_number(),
                            &bIsInstintatnt);

                nBufferIndex %= 2;
            }
        }
    }
    catch(...)
    {
        printf("error during receive!\n");
    }


}


void pitz::adc::remoteplotspectrum::MainWindow::StopSubscriberThread()
{
    if(!m_bConnected){return;}
    m_bConnected = false;
    m_subscriberThread.join();
}


void pitz::adc::remoteplotspectrum::MainWindow::ActionConnectDisconnectSlot()
{
    try{
        //
        if(m_bConnected)
        {
            m_actionConnect.setIcon(QIcon(":/img/connect_btn.png"));
            StopSubscriberThread();
            m_subscriber.disconnect();
        }
        else    {

            //if(m_connectDlg.execC(vcHostName,128,&nBoardNum,&nChanNum))
            {
                int nReceived;
                if(m_subscriber.connect(m_hostName.c_str(),m_nBoardNum,m_nChanNum)){
                    printf("Unable to connect!\n"); // should be messagebox
                    return;
                }
                nReceived = m_subscriber.recv_data(m_serializer.get_buffer(),adc_data_serializer::header_size);
                if(nReceived<adc_data_serializer::header_size){
                    printf("Unable to receive !\n"); // should be messagebox
                    return;
                }
                m_serializer.prepare_buffer_based_on_header();
                m_actionConnect.setIcon(QIcon(":/img/disconnect_btn.png"));
                m_bConnected = true;
                m_subscriberThread = std::thread(&MainWindow::SubscriberThreadFunc, this);
            }
        } // else  for if(m_bConnected)
    }// try{
    catch(...)
    {
        printf("Exception on zmq !\n");// messagebox should be
    }

}


void pitz::adc::remoteplotspectrum::MainWindow::ActionConnectDetailsSlot()
{
    m_connectDlg.execC(&m_hostName,&m_nBoardNum,&m_nChanNum);
}
