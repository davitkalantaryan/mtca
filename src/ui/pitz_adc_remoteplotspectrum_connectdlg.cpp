
// pitz_adc_remoteplotspectrum_connectdlg.cpp
// 2017 Jul 04

#include "pitz_adc_remoteplotspectrum_connectdlg.hpp"

#if 0
QGridLayout m_mainLayout;
QLabel      m_labelHostName;
QLineEdit   m_editHostName;
QLabel      m_labelBoadrdNumber;
QLineEdit   m_editBoardNumber;
QLabel      m_labelChannelNumber;
QLineEdit   m_editChannelNumber;
#endif

pitz::adc::remoteplotspectrum::ConnectDlg::ConnectDlg()
    :
      m_labelHostName(tr("Host name")),
      m_labelBoadrdNumber(tr("Board number")),
      m_labelChannelNumber(tr("Channel number")),
      m_ok(tr("OK")),
      m_cancel(tr("Cancel"))
{
    m_mainLayout.addWidget(&m_labelHostName,0,0);
    m_mainLayout.addWidget(&m_editHostName,0,1);

    m_mainLayout.addWidget(&m_labelBoadrdNumber,1,0);
    m_mainLayout.addWidget(&m_editBoardNumber,1,1);

    m_mainLayout.addWidget(&m_labelChannelNumber,2,0);
    m_mainLayout.addWidget(&m_editChannelNumber,2,1);

    m_mainLayout.addWidget(&m_ok,3,0);
    m_mainLayout.addWidget(&m_cancel,3,1);

    setLayout(&m_mainLayout);

    connect( &m_ok, SIGNAL(clicked()), this, SLOT(OkPushedSlot()) );
    connect( &m_cancel, SIGNAL(clicked()), this, SLOT(close()) );

}



pitz::adc::remoteplotspectrum::ConnectDlg::~ConnectDlg()
{
    m_mainLayout.removeWidget(&m_cancel);
    m_mainLayout.removeWidget(&m_ok);

    m_mainLayout.removeWidget(&m_editChannelNumber);
    m_mainLayout.removeWidget(&m_labelChannelNumber);

    m_mainLayout.removeWidget(&m_editBoardNumber);
    m_mainLayout.removeWidget(&m_labelBoadrdNumber);

    m_mainLayout.removeWidget(&m_editHostName);
    m_mainLayout.removeWidget(&m_labelHostName);
}


void pitz::adc::remoteplotspectrum::ConnectDlg::OkPushedSlot()
{
    m_bOkPushed=true;
    close();
}


bool pitz::adc::remoteplotspectrum::ConnectDlg::execC(
        std::string* a_hostName, int* a_boardNum, int* a_chanNum )
{
    m_bOkPushed = false;

    m_editHostName.setText(a_hostName->c_str());
    m_editBoardNumber.setValue(*a_boardNum);
    m_editChannelNumber.setValue(*a_chanNum);

    QDialog::exec();
#ifndef WIN32
    *a_hostName = m_editHostName.text().toStdString();
#endif
    *a_boardNum = m_editBoardNumber.value();
    *a_chanNum = m_editChannelNumber.value();

    return m_bOkPushed;
}
