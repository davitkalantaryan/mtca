
// 2017 Jul 04

#include "pitz_adc_remoteplotspectrum_application.hpp"
#include "pitz_adc_remoteplotspectrum_mainwindow.hpp"
#include <stdio.h>

int main(int argc, char* argv[])
{
    freopen( "/dev/null", "w", stderr);

    pitz::adc::remoteplotspectrum::Application app(argc, argv);
    pitz::adc::remoteplotspectrum::MainWindow aWindow;

    aWindow.show();

    app.exec();

    return 0;
}
