
// pitz_adc_remoteplotspectrum_centralwidget
// 2017 Jul 04

#ifndef PITZ_ADC_REMOTEPLOTSPECTRUM_CENTRALWIDGET_HPP
#define PITZ_ADC_REMOTEPLOTSPECTRUM_CENTRALWIDGET_HPP

#include <QWidget>
#include <QVBoxLayout>
#include "common_ui_qt_graphicwidget.hpp"

namespace pitz { namespace adc{ namespace remoteplotspectrum{

class CentralWidget : public QWidget
{
public:
    CentralWidget();
    ~CentralWidget();

    //int SetSpectrum()
    common::ui::qt::GraphicWidget& GraphWidget();

private:
    QVBoxLayout                     m_mainLayout;
    common::ui::qt::GraphicWidget   m_graphicWidget;
};

}}}

#endif // PITZ_ADC_REMOTEPLOTSPECTRUM_CENTRALWIDGET_HPP
