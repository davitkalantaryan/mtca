
// 2017 Jul 04

#ifndef __pitz_adc_remoteplotspectrum_application_hpp__
#define __pitz_adc_remoteplotspectrum_application_hpp__

#include <QApplication>

namespace pitz{ namespace adc{ namespace remoteplotspectrum{

typedef char* TypeCharPtr;

class Application : public QApplication
{
public:
    Application(int& argc, char* argv[]);
    ~Application();
};

}}}



#endif  // #ifndef __pitz_adc_remoteplotspectrum_application_hpp__
