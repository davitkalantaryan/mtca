
// 2017 Jul 04

#ifndef __pitz_adc_remoteplotspectrum_mainwindow_hpp__
#define __pitz_adc_remoteplotspectrum_mainwindow_hpp__

#include "common_ui_qt_mainwindow.hpp"
#include "pitz_adc_remoteplotspectrum_centralwidget.hpp"
#include "adc_data_serializer.h"
#include "zmqsubscriber.h"
#include "pitz_adc_remoteplotspectrum_connectdlg.hpp"
#include <thread>
#include <string>

namespace pitz{ namespace adc{ namespace remoteplotspectrum{


class MainWindow : public common::ui::qt::MainWindow
{
    Q_OBJECT
public:
    MainWindow();
    ~MainWindow();

private slots:
    void ActionConnectDisconnectSlot();
    void ActionConnectDetailsSlot();

private:
    void SubscriberThreadFunc();
    void StopSubscriberThread();

private:
    CentralWidget       m_centrawWidget;
    QAction             m_actionConnect;
    QAction             m_actionConnectinDetails;
    ConnectDlg          m_connectDlg;

    bool                m_bConnected;
    ZmqSubscriber       m_subscriber;
    adc_data_serializer m_serializer;
    std::thread         m_subscriberThread;
    std::string         m_hostName;
    int                 m_nBoardNum;
    int                 m_nChanNum;
};

}}}


#endif // #ifndef __pitz_adc_remoteplotspectrum_mainwindow_hpp__

