
// pitz_adc_remoteplotspectrum_connectdlg.hpp
// 2017 Jul 04

#ifndef PITZ_ADC_REMOTEPLOTSPECTRUM_CONNECTDLG_HPP
#define PITZ_ADC_REMOTEPLOTSPECTRUM_CONNECTDLG_HPP

#include <QDialog>
#include <QLineEdit>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QSpinBox>
#include <string>

namespace pitz{ namespace adc{ namespace remoteplotspectrum{

class ConnectDlg : private QDialog
{
    Q_OBJECT
public:
    ConnectDlg();
    ~ConnectDlg();

    bool execC(std::string* hostName, int* boardNum, int* chanNum );

private slots:
    void OkPushedSlot();

private:
    QGridLayout m_mainLayout;
    QLabel      m_labelHostName;
    QLineEdit   m_editHostName;
    QLabel      m_labelBoadrdNumber;
    QSpinBox    m_editBoardNumber;
    QLabel      m_labelChannelNumber;
    QSpinBox    m_editChannelNumber;
    QPushButton m_ok;
    QPushButton m_cancel;

    bool        m_bOkPushed;
};

}}}

#endif // PITZ_ADC_REMOTEPLOTSPECTRUM_CONNECTDLG_HPP
