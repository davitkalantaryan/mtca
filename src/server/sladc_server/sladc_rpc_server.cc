// file sladc_rpc_server.cc 
//
// $Date: 2009/12/08 14:40:00 $
// $Source: /doocs/doocssvr1/cvsroot/source/server/pitz/sladc/sladc_rpc_server.cc,v $
// $Revision: 1.1 $
// $State: Exp $
//
// Author:      Bagrat Petrosyan (geg) <Bagrat.Petrosyan@desy.de>
//              Copyright  2002 Bagrat Petrosyan
//
//              This program is free software; you can redistribute it
//              and/or  modify it under  the terms of  the GNU General
//              Public  License as  published  by  the  Free  Software
//              Foundation;  either  version 2 of the License, or  (at
//              your option) any later version.
//
//
// $Log: sladc_rpc_server.cc,v $
// Revision 1.1  2009/12/08 14:40:00  bagrat
// *** empty log message ***
//
// Revision 1.4  1998/12/01 15:31:10  cobraadm
// for serverlib 3.2.13 arguments for interrupt_usr1_prolog and
// interrupt_usr1_epilog needed
//
// Revision 1.3  1997/12/09 13:56:59  cobraadm
// change to alias_("ALIAS device name"),
//
// Revision 1.2  1997/12/05 12:54:36  ohensler
// Some small additions
//
// Revision 1.1  1997/12/03 17:37:02  cobraadm
// initial version of automatic server generation
//
//
//
//
//
//
//

#include <unistd.h>
#include	"eq_sladc.h"
#include	"eq_errors.h"

#if 1
#define DEBUG_SLADC(...) do{}while(0)
#else
#define DEBUG_SLADC(...) \
    do{printf("fnc:%s, ln:%d ->",__FUNCTION__,__LINE__);printf(__VA_ARGS__);}while(0)
#endif

const char*			object_name = "SLOW_ADC"; // name of this object (used in error messages)
extern  int		ring_buffer;
time_t			walltime;

EqFctSladc::EqFctSladc()
	: EqFct ("NAME = location")
	, alias_("ALIAS device name", this)

	, read_time_("RATE reading rate in sec", this)
	, resolution_("RESOLUTION of acd in bits", this)
	, deviceName_("DEVICE_NAME name of device", this)
	, deviceBar_("DEVICE_BAR of mmap", this)
	, deviceOffset_("VME_ADDR in carier", this)
	, needSwap_("NEED_SWAP true->1, false->0", this)
{

	int		i;
	char	msg[80];
	
	for (i = 0; i < CHAN_NUM; i++)
	{
		sprintf(msg, "CH%i.DESC description of channel \"%d\"", i, i);
		desc_[i] = new D_ustr(msg, this);
		
		sprintf(msg, "CH%i.ROW row data of channel \"%d\"", i, i);
		rowdata_[i] = new D_SladcWord(msg, 4*i, this);
		
		sprintf(msg, "CH%i.POLYPARA polipara of channel \"%d\"", i, i);
		calcalg_[i] = new D_polynom(msg, this);
		
		sprintf(msg, "CH%i.DATA data of channel \"%d\"", i, i);
		calcdata_[i] = new D_SladcFloat(msg, this);
		
		sprintf(msg, "CH%i.DATA.HIST patm of channel \"%d\"", i, i);
		calcdata_hist_[i] = new D_hist(this, msg, DATA_A_TS_FLOAT, ring_buffer, 1);
	}
}

void	eq_init_prolog()	// called before init of all EqFct's
{
	set_arch_mode(1);
}

void	EqFctSladc::init()
{
	int16_t resolusopn_offset; // resolution register offset specific
	if (needSwap_.value() == EqFctSladc::NEED_SWAP) {
		specific_offset_low = 2;
		specific_offset_high = 0;
		resolusopn_offset = 0x8A;
	}
	else {
		specific_offset_low = 3;
		specific_offset_high = 1;
		resolusopn_offset = 0x8B;
	}
	base_addr_ = (char*)EqFctSladc::mMapper.openAndMapC(deviceName_.value(), deviceBar_.value());
	if (base_addr_ == MAP_FAILED)
	{
		printftostderr(name_str(), "mmap failed for device \"%s\"", deviceName_.value());
		g_sts_.online(0);
	}
	else
	{
		base_addr_ += deviceOffset_.value();
		g_sts_.online(1);
		switch (*(base_addr_ + resolusopn_offset))
		{
		case 0x07: is16bit = true; resolution_.copy_value("16 bit"); break;
		case 0x08: is16bit = false; resolution_.copy_value("12 bit"); break;
		default: printf("failed to access resolution register\n"); exit(1);
		}
		printftostderr(name_str(), "mmap for device \"%s\" successful", deviceName_.value());
	} 

	set_error(no_error);
	needSwap_.set_ro_access();
}

void	eq_init_epilog()	// called at end of init of all EqFct's
{
	set_arch_mode(1); 
}

EqFct *eq_create(int code, void *)
{
	EqFct *eqn;
	switch (code)
	{
	case CodeSladc:
		eqn =  new EqFctSladc();
		break;
	default:
		printf("bad eq_fct number\n");
		eqn = (EqFct *) 0;
		break;
	}
	return eqn;
}

void	EqFctSladc::update()
{
	int tiv; float val;


		
	if (g_sts_.online()) {
		
		if (read_adc(read_time_.value()) != OK) return;

        for (int i = 0; i < CHAN_NUM; i++)
                {
                    tiv = rowdata_[i]->value();
                    rowdata_[i]->set_value(tiv);
                    val = calcalg_[i]->evaluate(tiv);
                    calcdata_[i]->set_value(val);
                    calcdata_hist_[i]->fill_hist(val, sts_ok, walltime, 0);
                    DEBUG_SLADC("ch%d->%x ", i, tiv);
                }
	
//		for (int i = 0; i < CHAN_NUM; i++)
//		{
//			tiv = rowdata_[i]->value();
//			rowdata_[i]->set_value(tiv);
//			val = calcalg_[i]->evaluate(tiv);
//			calcdata_[i]->set_value(val);
//			calcdata_hist_[i]->fill_hist(val, sts_ok, walltime, 0);
//			printf("ch%d->%x ", i, tiv);
//		} printf("\n");
	} // if online

	    //
	    //	device is offline
	    //
	else {
		set_error((int)went_offline);
		for (int i = 0; i < CHAN_NUM; i++)
		{
			tiv = 0;
			calcdata_[i]->set_value(0);
			calcdata_hist_[i]->fill_hist(0, sts_err, walltime, 0);
		}
		init();
		usleep(1000000);
	}
}

void refresh_prolog()		// called before "update"
{
	walltime =:: time(0);
}

void refresh_epilog()		// called after "update"
{
}

// the external interrupts functions

void interrupt_usr1_prolog(int i)
{
}

void interrupt_usr1_epilog(int i)
{
}

void interrupt_usr2_prolog()
{
}

void interrupt_usr2_epilog()
{
}

void post_init_prolog()
{
}

void post_init_epilog()
{
}

void eq_cancel()
{
}

int D_SladcWord::value()
{
	if (!eq_fct_->base_addr_) return -1;
	uint8_t bytes[2];
	bytes[0] = *(eq_fct_->base_addr_ + offset_ + eq_fct_->specific_offset_low);
	bytes[1] = *(eq_fct_->base_addr_ + offset_ + eq_fct_->specific_offset_high);
	if (eq_fct_->is16bit == false)
		return (((bytes[1] << 8) + bytes[0]) >> 4);
	else
		return ((bytes[1] << 8) + bytes[0]);
}


int EqFctSladc::read_adc(int time)
{
	int diff; uint8_t sts; 
	
	diff = walltime - oldtime;
	if (abs(diff) < time)
	{
		return NOTREADY;
	}
	oldtime = walltime;
	
	sts = *(base_addr_ + 0x22 + specific_offset_high); //To know the state of adc
	if (sts & 1 == 1) 
	{
		printf("case busy...\n");
		return BUSY;
	}

	if (time) *(base_addr_ + 0x20 + specific_offset_high) = 1; //soft trigger adc
	
	return OK;
}



