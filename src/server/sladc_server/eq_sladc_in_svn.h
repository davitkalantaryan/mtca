// file eq_Sladc.h
//
// $Date: 2009/12/08 14:39:59 $
// $Source: /doocs/doocssvr1/cvsroot/source/server/pitz/sladc/eq_sladc.h,v $
// $Revision: 1.1 $
// $State: Exp $
//
// Author:      Bagrat Petrosyan (geg) <Bagrat.Petrosyan@desy.de>
//              Copyright  2002 Bagrat Petrosyan
//
//              This program is free software; you can redistribute it
//              and/or  modify it under  the terms of  the GNU General
//              Public  License as  published  by  the  Free  Software
//              Foundation;  either  version 2 of the License, or  (at
//              your option) any later version.
//
//
/* 
 * $Log: eq_sladc.h,v $
 * Revision 1.1  2009/12/08 14:39:59  bagrat
 * *** empty log message ***
 *
 * Revision 1.2  1997/12/05 14:08:13  cobraadm
 * add define of CodeSladc to eq_Sladc.h and creation of the value for
 * CodeSladc in create_server
 *
 * Revision 1.1  1997/12/03 17:37:08  cobraadm
 * initial version of automatic server generation
 *
 *
 *
 *
 *
 */

#ifndef eq_Sladc_h
#define eq_Sladc_h

#include	<eq_fct.h>
#include <eq_fct_errors.h>
#include <mtca_devregmapper.hpp>

#define 	CodeSladc	300
#define CHAN_NUM		8
#define MAX_MODULE		4

#define NOTREADY		1
#define BUSY			2
#define OK				3

class EqFctSladc;

class D_SladcWord : public D_int
{
	protected:
	u_short rowval_;
	u_char offset_;
	EqFctSladc*	eq_fct_;
	
public:
	D_SladcWord(char* pn, u_char offs, EqFctSladc* efp)
		: D_int(pn, (EqFct*)efp)
		, offset_(offs)
		, eq_fct_(efp) {}
	
	int value();
};

class D_SladcRate : public D_int
{
public:
	D_SladcRate(char* pn, EqFct* eq)
		: D_int(pn, eq){}
	void set(EqAdr *, EqData *fromClient, EqData *toClient, EqFct *)
	{
		int nTiv = fromClient->get_int();
		if (nTiv < 0 || nTiv > 60)
		{
			toClient->error(write_error, "range error");
		}
		else
		{
			value_ = nTiv;
		}
	}
};

class D_SladcFloat : public D_float
{
protected:
	EqFctSladc*	eq_fct_;
	
public:
	D_SladcFloat(char* pn, EqFctSladc* efp):
		D_float(pn, (EqFct*)efp), eq_fct_(efp){};
};

class EqFctSladc : public EqFct {
	friend class D_SladcWord;
	enum
	{
		NEED_SWAP,
		NO_SWAP
	};
	
protected:

	D_name			alias_;
	
        bool is16bit;

	D_int			read_time_;
	D_string        resolution_;
	D_string		deviceName_;
	D_int			deviceBar_;
	D_int			deviceOffset_;
	D_int			needSwap_;
	
	D_ustr			*desc_[CHAN_NUM];
	D_SladcWord		*rowdata_[CHAN_NUM];
	D_polynom		*calcalg_[CHAN_NUM];
	D_SladcFloat	*calcdata_[CHAN_NUM];
	D_hist			*calcdata_hist_[CHAN_NUM];
	int get_specific_offset()
	{
		if (needSwap_.value() == EqFctSladc::NEED_SWAP) {
			return 0;
		}
		else {
			return 1;
		}
	}
    
public:
	mtca::DevRegMapper mMapper;

	caddr_t			base_addr_;
	time_t			oldtime;

	EqFctSladc ( );		// constructor
	~EqFctSladc ( ) {}	// destructor

	void	update ();
	void	init ();
	int	fct_code()	{ return CodeSladc; };

	int read_adc(int);
	int specific_offset_;

};


#endif
