#!/bin/sh
#
# file: local_create_mtca_account_prvt
#
# $Date 2015/09/29 11:11 $
#
# purpose:	to create local account on MTCA similar to network account
#
# author:	Davit Kalantaryan  (davit.kalantaryan@desy.de)
# 
# arguments
# @1 New user name
# @2 MTCA administrative password
#


#jddd_run /doocs/develop/kalantar/XML/sis8300/sis8300_main_wnd.xml 2>/dev/null 1>/dev/null
jddd_run ./sis8300_main_wnd.xml 2>/dev/null 1>/dev/null

exit 0
