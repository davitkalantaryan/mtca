/*****************************************************************************
 * File		  main_pcielinkerrorinvestigations.cpp
 * created on 2017-05-20
 *****************************************************************************
 * Author:	D.Kalantaryan, Tel:033762/77552 kalantar
 * Email:	davit.kalantaryan@desy.de
 * Mail:	DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 *   file for ...
 ****************************************************************************/

#define     _EVENT_LOW_REG_ADDR_        0x40
#define     _EVENT_HIGH_REG_ADDR_       0x42

#define     _LOG_MAX_SIZE_              20000000 // 20 MB
#define     _LOG_SIZE_AFTER_CUT_        10000000 // 10 MB

#ifdef WIN32
#include <windows.h>
#include <io.h>
#include <process.h>
#define CHANGE_SIZE( __file2__, __size2__ )     chsize(fileno((__file2__)),(__size2__))
#else
#include <unistd.h>
#include <sys/types.h>
#define Sleep(_x_) usleep(1000*(_x_))
#define CHANGE_SIZE( __file2__, __size2__ )     ftruncate(fileno((__file2__)),((off_t)(__size2__)))
#endif

#include <stdlib.h>
#include <stdio.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <string.h>
#include "mtca_upciedevlocal.hpp"

static bool s_bLogFileExist=false;
static int  WriteLog(FILE* a_logFile, const char* a_cpcFormat,...);

int main(int argc, char* argv[])
{
    const char* cpcDevName;
    FILE* fpLogFile;
    mtca::UpciedevLocal aTimerCore;
    u_int32_t unEventNum;
    int nCarrier, nMainOffset;
    int nSleepTimeMs;
    char vcLogFileName[128];

    printf("version: 7\n");
    if(argc<3){
        fprintf(stderr,"provide the device entry and IP carier for test!\n");
        return 1;
    }

    nCarrier = atoi(argv[2]);
    nMainOffset = nCarrier*0x100;
    printf("device entry: \"%s\", carrier:%d, MainOffset=%d\n",argv[1],nCarrier,nMainOffset);

#if 1
#ifdef WIN32
#else  // #ifdef WIN32
    if( fork() )exit( 0);
    setsid();
    umask(0);
    if( fork() )exit( 0);
#endif // #ifdef WIN32
#endif // #if 0/1

    if(aTimerCore.open(argv[1])<0){
        fprintf(stderr,"unable to open the file!\n");
        return 2;
    }

    cpcDevName = strrchr(argv[1],'/');
    cpcDevName = cpcDevName?cpcDevName:strrchr(argv[1],'\\');
    cpcDevName=cpcDevName?(cpcDevName+1):"_";
    snprintf(vcLogFileName,127,"testerlog_%s_%d.log",cpcDevName,nCarrier);
    fpLogFile = fopen(vcLogFileName,"a+");
    if(!fpLogFile){fpLogFile=stdout;s_bLogFileExist=false;}
    else{s_bLogFileExist=true;}

    while(true){
        aTimerCore.read(3,nMainOffset+_EVENT_LOW_REG_ADDR_,2,&unEventNum,RW_D16);
        nSleepTimeMs=rand()%10;
        WriteLog(fpLogFile,"eventNum=%d, dev=\"%s\", carrier=%d, sleepTime=%d\n",
               (int)unEventNum,argv[1],nCarrier,nSleepTimeMs);
        Sleep(nSleepTimeMs);
    }

    if(s_bLogFileExist){fclose(fpLogFile);s_bLogFileExist=false;}
    return 0;
}


static int WriteLog(FILE* a_logFile, const char* a_cpcFormat,...)
{
    va_list argList;
    struct stat fStat;
    int nRet( -1 );

    if(!a_logFile){return -1;}
    if( fstat( fileno( a_logFile ), &fStat ) ){return -2;}
    if((fStat.st_size>_LOG_MAX_SIZE_)&& s_bLogFileExist){CHANGE_SIZE(a_logFile,_LOG_SIZE_AFTER_CUT_);}

    va_start(argList, a_cpcFormat);
    nRet=vfprintf (a_logFile, a_cpcFormat, argList);
    va_end(argList);

    return nRet;
}
