/*
 *	File: adc_shared_memory.cpp
 *
 *	Created on: Mar 10, 2015
 *	Authors:
 *      Bagrat Petrosyan  (Email: bagrat.petrosyan@desy.de)
 *      Davit Kalantaryan (Email: davit.kalantaryan@desy.de)
 *
 *
 */

#define _DEPRICATED_FUNC_

#include "adc_shared_memory.h"
#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ioctl.h>

#if 0
int     m_fd;
void*   m_mmaped_memory;
int     m_nNumberOfSamples;
int     m_nOneChannelSize;
int     m_nOneBufferSize;
//int     m_nWholeMemSize; // It is used twice during creation and decreation
#endif


adc_shared_memory::adc_shared_memory()
    : m_mmaped_memory(NULL),
      m_fd(-1),
      m_nNumberOfSamples(0),
      m_nOneChannelSize(0),
      m_nOneBufferSize(0)
      //,m_nWholeMemSize(0),
{}


adc_shared_memory::adc_shared_memory(const char* a_dev_name)
    : m_mmaped_memory(NULL),
      m_fd(-1),
      m_nNumberOfSamples(0),
      m_nOneChannelSize(0),
      m_nOneBufferSize(0)
      //,m_nWholeMemSize(0),
{
    if(connect_to_shared_memory(a_dev_name)<0)
    {
        throw "failed to connect to shared memory!";
    }
}


adc_shared_memory::~adc_shared_memory()
{
    disconnect_from_shared_memory();
}


int adc_shared_memory::connect_to_shared_memory(const char* a_dev_name)
{
    disconnect_from_shared_memory();
    m_fd = open(a_dev_name,O_RDWR);
    if(m_fd>0)
    {
        //mmap(NULL, nSizeOfSharedMemory, PROT_READ | PROT_WRITE, MAP_SHARED, 0);
        m_nNumberOfSamples = ioctl(m_fd,ADC_NUMBER_OF_SAMPLES);
        m_nOneChannelSize = ONE_CHANNEL_SIZE(m_nNumberOfSamples);
        m_nOneBufferSize = ONE_BUFFER_SIZE(DMA_TRANSFER_LEN(m_nNumberOfSamples));
        //m_nWholeMemSize = WHOLE_MEMORY_SIZE(m_nOneBufferSize);
        m_mmaped_memory = mmap(NULL,WHOLE_MEMORY_SIZE(m_nOneBufferSize),PROT_READ,MAP_SHARED,m_fd,0); // ONE_CHANNEL_SIZE
        if(m_mmaped_memory == MAP_FAILED)
        {
            //m_nWholeMemSize = 0;
            m_nOneBufferSize = 0;
            m_nOneChannelSize = 0;
            m_nNumberOfSamples = 0;
            m_mmaped_memory = NULL;
            close(m_fd);
            m_fd = -1;
            return -2;
        }

    }


    return m_fd;
}


void adc_shared_memory::disconnect_from_shared_memory()
{
    if(m_fd>=0)
    {
        if(m_mmaped_memory){munmap(m_mmaped_memory,WHOLE_MEMORY_SIZE(m_nOneBufferSize)); m_mmaped_memory = NULL;}
        close(m_fd);
        m_fd = -1;
        //m_nWholeMemSize = 0;
        m_nOneBufferSize = 0;
        m_nOneChannelSize = 0;
        m_nNumberOfSamples = 0;
    }
}



int adc_shared_memory::number_of_samples()const
{
    //m_nNumberOfSamples = ioctl(m_fd,ADC_NUMBER_OF_SAMPLES);
    return m_nNumberOfSamples;
}


int adc_shared_memory::slot_number()const
{
    return ioctl(m_fd,PCIEDEV_GET_SLOT_NUMBER);
}


int adc_shared_memory::event_number_in_specific_buffer(const void* a_spec_buff)
{
    return *((const int*)((const char*)a_spec_buff + __OFSET_TO_EVENT_NUMBER__));
}


int adc_shared_memory::wait_for_data(int a_nWaitTimeMS)
{
    int nReturn = ioctl (m_fd,ADC_WAIT_FOR_DMA_TIMEOUT,&a_nWaitTimeMS);
    return nReturn;
}


int adc_shared_memory::wait_for_data_inf()
{
    int nReturn = ioctl (m_fd,ADC_WAIT_FOR_DMA_INF);
    return nReturn;
}



const void* adc_shared_memory::current_buffer_pointer()const
{
    int nBufIndex = CURRENT_BUF_INDEX(m_mmaped_memory);
    return specific_buffer_pointer(nBufIndex);
}


const void* adc_shared_memory::specific_buffer_pointer(int a_buffer_index)const
{
    int offset_to_buffer = OFFSET_TO_BUFFER(a_buffer_index,m_nOneBufferSize);
    return ((char*)m_mmaped_memory) + offset_to_buffer;
}


const void* adc_shared_memory::current_buffer_specific_channel_pointer(int a_channel_number)const
{
    int nBufIndex = CURRENT_BUF_INDEX(m_mmaped_memory );
    return specific_buffer_specific_channel_pointer(nBufIndex,a_channel_number);
}


const void* adc_shared_memory::specific_buffer_specific_channel_pointer(int a_buffer_index,int a_channel_number)const
{
    int offset_to_buffer = OFFSET_TO_BUFFER(a_buffer_index,m_nOneBufferSize);
    int offset_to_channel = __HEADER_SIZE__+offset_to_buffer + a_channel_number*m_nOneChannelSize;
    return ((char*)m_mmaped_memory) + offset_to_channel;
}


const void* adc_shared_memory::specific_buffer_specific_channel_pointer_new(const void* a_spec_buff,int a_channel_number)const
{
    return CHANNEL_PTR_IN_BUFFER(a_spec_buff,a_channel_number,m_nOneChannelSize);
}
