#include <adc_data_serializer.h>
#include <cstdlib>

#define CURRENT_VERSION 1

#define VALUE_IN_THE_OFFSET(_a_type,_a_offset)	*((_a_type*)(data_buffer+(_a_offset)))

#define UINT_VALUE_IN_THE_OFFSET2(_a_offset)	VALUE_IN_THE_OFFSET(int32_t,_a_offset)

#ifdef BIG_ENDIAN_BP
#define SWAP_AND_SET_INT(_a_offset,_a_value)	swap_4_byte(&(_a_value));\
												UINT_VALUE_IN_THE_OFFSET2(_a_offset)=(_a_value)
#define GET_AND_SWAP_INT(_a_offset,_a_value)	(_a_value) = UINT_VALUE_IN_THE_OFFSET2(_a_offset);\
												swap_4_byte(&(_a_value));
#else   // #ifdef BIG_ENDIAN_BP
#define SWAP_AND_SET_INT(_a_offset,_a_value)	UINT_VALUE_IN_THE_OFFSET2(_a_offset)=(_a_value)
#define GET_AND_SWAP_INT(_a_offset,_a_value)	(_a_value) = UINT_VALUE_IN_THE_OFFSET2(_a_offset);
#endif  // #ifdef BIG_ENDIAN_BP


/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
	
void* adc_data_serializer::swap_4_byte(void* a_src)
{
	*((uint32_t*)a_src) = (((*((uint32_t*)a_src)) & 0xff000000) >> 24) |
						  (((*((uint32_t*)a_src)) & 0x00ff0000) >>  8) |
						  (((*((uint32_t*)a_src)) & 0x0000ff00) <<  8) |
						  (((*((uint32_t*)a_src)) & 0x000000ff) <<  24);
	return a_src;
}
	
void* adc_data_serializer::swap_8_byte(void* a_src)
{
	*((uint64_t*)a_src) =  (((*((uint64_t*)a_src)) & 0xff00000000000000ull) >> 56) |
						   (((*((uint64_t*)a_src)) & 0x00ff000000000000ull) >> 40) |
						   (((*((uint64_t*)a_src)) & 0x0000ff0000000000ull) >> 24) |
						   (((*((uint64_t*)a_src)) & 0x000000ff00000000ull) >> 8) |
						   (((*((uint64_t*)a_src)) & 0x00000000ff000000ull) << 8) |
						   (((*((uint64_t*)a_src)) & 0x0000000000ff0000ull) << 24) |
						   (((*((uint64_t*)a_src)) & 0x000000000000ff00ull) << 40) |
						   (((*((uint64_t*)a_src)) & 0x00000000000000ffull) << 56);
	return a_src;
}

	/////////////////////////////////////////////////////////////////////////////////
	// constructor ( creates float type )
adc_data_serializer::adc_data_serializer()
	: m_swap_bytes(&swap_4_byte)
{
	uint32_t nTmp;

	data_buffer = (uint8_t*)malloc(header_size);
	if (!data_buffer)
	{
		throw "failed to allocate memory for data buffer";
	}

	nTmp = CURRENT_VERSION;
	SWAP_AND_SET_INT(ADC_SERIALIZER::OFFSET_VERSION, nTmp);

	// set type float
	nTmp = ADC_SERIALIZER::ARAY_DATA_FLOAT;	SWAP_AND_SET_INT(ADC_SERIALIZER::OFFSET_TYPE, nTmp);	set_spectrum_size(0);
	
	// set start marker
	data_buffer[0] = 'B'; 
	data_buffer[1] = 'E'; 
	data_buffer[2] = 'G'; 
	data_buffer[3] = '0'; 
}
	
// destructor
adc_data_serializer::~adc_data_serializer()
{
	free(data_buffer);
}


	
//////////////////////////////////////////////////////////////////////////////////
// header set API
// set serializer version
#if 0   // Serializer version should be set automatically
void adc_data_serializer::set_version(int32_t aVersion)
{
#ifdef BIG_ENDIAN_BP
	swap_4_byte(&aVersion);
#endif	*((int32_t*)(data_buffer + ADC_SERIALIZER::OFFSET_VERSION)) = aVersion;
}
#endif

	// set spectrum type: float -> 0, double -> 1
void adc_data_serializer::set_spectrum_type(int32_t aSpectrumType)
{
	switch (aSpectrumType)
	{
	case ADC_SERIALIZER::ARAY_DATA_FLOAT:
		m_swap_bytes = &swap_4_byte;
		break;
	case ADC_SERIALIZER::ARAY_DATA_DOUBLE:
		m_swap_bytes = &swap_8_byte;
		break;
	default:
		throw "unsupported spectrum type encountered";
		break;
	}
	SWAP_AND_SET_INT(ADC_SERIALIZER::OFFSET_TYPE,aSpectrumType);	set_spectrum_size(get_spectrum_size());	
}


		// set spectrum size
void adc_data_serializer::set_spectrum_size(int32_t aSpectrumSize)
{
	switch (get_spectrum_type())
	{
	case ADC_SERIALIZER::ARAY_DATA_FLOAT:
		data_buffer = (uint8_t*)realloc(data_buffer, header_size + aSpectrumSize * sizeof(float));
		break;
	case ADC_SERIALIZER::ARAY_DATA_DOUBLE:
		data_buffer = (uint8_t*)realloc(data_buffer, header_size + aSpectrumSize * sizeof(double));
		break;
	default:
		throw "unsupported spectrum type encountered";
		break;
	}
	if (!data_buffer)
	{
		throw "failed to reallocate memory for data buffer";
	}

	SWAP_AND_SET_INT(ADC_SERIALIZER::OFFSET_SIZE,aSpectrumSize);
}

	// set event number to internal buffer
void adc_data_serializer::set_event_number(int32_t aEventNumber)
{
	SWAP_AND_SET_INT(ADC_SERIALIZER::OFFSET_EVENT,aEventNumber);
}

	// set event time seconds
void adc_data_serializer::set_time_seconds(int32_t aSeconds)
{
	SWAP_AND_SET_INT(ADC_SERIALIZER::OFFSET_SECONDS, aSeconds);
}

void adc_data_serializer::set_time_useconds(int32_t aUSeconds)
{
	SWAP_AND_SET_INT( ADC_SERIALIZER::OFFSET_USECONDS, aUSeconds);
}

//////////////////////////////////////////////////////////////////////////////////
// header get API
// get serializer version
int32_t adc_data_serializer::get_version()const
{
	int32_t nRet;
	GET_AND_SWAP_INT(ADC_SERIALIZER::OFFSET_VERSION, nRet);
	return nRet;}

	
// get spectrum type
int32_t adc_data_serializer::get_spectrum_type()const
{
	int32_t nRet;
	GET_AND_SWAP_INT(ADC_SERIALIZER::OFFSET_TYPE, nRet);
	return nRet;}

// get header size
int32_t adc_data_serializer::get_header_size()const
{
	return header_size;}

// get spectrum size
int32_t adc_data_serializer::get_spectrum_size()const
{
	int32_t nRet;
	GET_AND_SWAP_INT(ADC_SERIALIZER::OFFSET_SIZE, nRet);
	return nRet;}

// get buffer size
int32_t adc_data_serializer::get_buffer_size()const
{
	int32_t nRet;
	GET_AND_SWAP_INT(ADC_SERIALIZER::OFFSET_SIZE, nRet);
	if (get_spectrum_type() == ADC_SERIALIZER::ARAY_DATA_FLOAT)
		return nRet * sizeof(float) + header_size;
	else
		return nRet * sizeof(double) + header_size;
}

// get event number
int32_t adc_data_serializer::get_event_number()const
{
	int32_t nRet;
	GET_AND_SWAP_INT(ADC_SERIALIZER::OFFSET_EVENT, nRet);
	return nRet;}


// get time seconds
int32_t adc_data_serializer::get_time_seconds()const
{
	int32_t nRet;
	GET_AND_SWAP_INT(ADC_SERIALIZER::OFFSET_SECONDS, nRet);
	return nRet;}
	
// get time microseconds
int32_t adc_data_serializer::get_time_useconds()const
{
	int32_t nRet;
	GET_AND_SWAP_INT(ADC_SERIALIZER::OFFSET_USECONDS, nRet);
	return nRet;}

// get data buffer
// warning: must be called after void adc_data_serializer::prepare_spectrum()
const uint8_t* adc_data_serializer::get_buffer()const
{
	return data_buffer;
}
// warning: must be called after void adc_data_serializer::prepare_spectrum()
uint8_t* adc_data_serializer::get_buffer()
{
	return data_buffer;
}

// returns content of data buffer as std string
// warning: must be called after void adc_data_serializer::prepare_spectrum()
#include <sstream>
std::string adc_data_serializer::toString()
{
	std::ostringstream oss;
	int32_t n_spec_size = get_spectrum_size();
	int32_t n_spec_type = get_spectrum_type();
	oss << "version " << get_version() << ", type "  << n_spec_type << ", size " << n_spec_size << ", event " << get_event_number() << ", seconds " << get_time_seconds() << ", microseconds " << get_time_useconds() << "\n";
	
	const void* dataBuffer = (const void*)(get_buffer() + header_size);
	
	if (n_spec_type == ADC_SERIALIZER::ARAY_DATA_FLOAT)
	{
		const float* dataBuffer = (const float*)(get_buffer() + header_size);
		for (int i(0); i < n_spec_size; ++i)
		{
			oss << "item " << i << " = " << get_spectrum_item<float>(i) << "; ";
		}
	}
	else
	{
		const double* dataBuffer = (const double*)(get_buffer() + header_size);
		for (int i(0); i < n_spec_size; ++i)
		{
			oss << "item " << i << " = " << get_spectrum_item<double>(i) << "; ";
		}
	}
	return oss.str();
}
