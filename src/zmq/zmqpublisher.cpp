#include "zmqpublisher.h"
#include <zmq.h>
#include <cerrno>
#include <cstring>
#include <cstdlib>

ZmqPublisher::ZmqPublisher(int slot_num, int channel_num) :
	BaseZmqSocket()
{
	m_zmq_socket = zmq_socket(BaseZmqSocket::smv_zmq_context, ZMQ_PUB);
	if (!m_zmq_socket)
	{
		throw (const char*)(std::strerror(errno));
	}
	char* pChar = (char*)std::malloc(256);
	snprintf(pChar, 256, "tcp://*:1%02d%02d", slot_num, channel_num);	
	if (zmq_bind(m_zmq_socket, pChar))
	{
		std::free(pChar);
		throw (const char*)(std::strerror(errno));
	}
	std::free(pChar);
}

ZmqPublisher::~ZmqPublisher()
{	
	if (m_zmq_socket)
	{
		zmq_close(m_zmq_socket);
		m_zmq_socket = nullptr;	
	}
}

int ZmqPublisher::send_data(const void* aData, int aSize)
{
	int nRet( zmq_send(m_zmq_socket, aData, aSize, 0));
	if ( nRet == -1)
	{
		throw (const char*)(std::strerror(errno));
	}
	return nRet;
}