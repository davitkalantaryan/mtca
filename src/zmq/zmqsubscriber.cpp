#include "zmqsubscriber.h"
#include <zmq.h>
#include <cerrno>
#include <cstring>
#include <cstdlib>


ZmqSubscriber::ZmqSubscriber(const char* aHostName, int slot_num, int channel_num, int aTimeout /*millisecond*/)
	:
	BaseZmqSocket(), mn_timeout(aTimeout)
{
	m_zmq_socket = zmq_socket(BaseZmqSocket::smv_zmq_context, ZMQ_SUB);
	if (!m_zmq_socket)
	{
		throw (const char*)(std::strerror(errno));
	}
	if (zmq_setsockopt(m_zmq_socket, ZMQ_SUBSCRIBE, "", 0))
	{
		throw (const char*)(std::strerror(errno));
	}
	if (zmq_setsockopt(m_zmq_socket, ZMQ_RCVTIMEO, &mn_timeout, sizeof(int)))
	{
		throw (const char*)(std::strerror(errno));
	}
	char* pChar = (char*)std::malloc(256);
	snprintf(pChar, 256, "tcp://%s:1%02d%02d", aHostName, slot_num, channel_num);	
	if (zmq_connect(m_zmq_socket, pChar))
	{
		std::free(pChar);
		throw (const char*)(std::strerror(errno));
	}
	std::free(pChar);	
}


ZmqSubscriber::~ZmqSubscriber()
{
	if (m_zmq_socket)
	{
		zmq_close(m_zmq_socket);
		m_zmq_socket = nullptr;	
	}
}

int ZmqSubscriber::recv_data(void* aData, int aSize)
{
	int nRet = zmq_recv(m_zmq_socket, aData, aSize, 0);
	if (nRet == -1)
	{
		throw (const char*)(std::strerror(errno));
	}
	return nRet;
}
