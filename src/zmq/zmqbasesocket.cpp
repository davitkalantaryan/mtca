#include "zmqbasesocket.h"

#include <zmq.h>

void* BaseZmqSocket::smv_zmq_context = nullptr;
int BaseZmqSocket::smn_instances = 0;

BaseZmqSocket::BaseZmqSocket()
{
	if (!BaseZmqSocket::smn_instances)
	{
		BaseZmqSocket::smv_zmq_context = zmq_ctx_new();
		if (!BaseZmqSocket::smv_zmq_context)
		{
			throw "failed to create 0MQ context";
		}
	}
	++BaseZmqSocket::smn_instances;
	m_zmq_socket = nullptr;
}

BaseZmqSocket::~BaseZmqSocket()
{
	--BaseZmqSocket::smn_instances;
	if (!BaseZmqSocket::smn_instances)
	{
		zmq_ctx_destroy(BaseZmqSocket::smv_zmq_context);
		BaseZmqSocket::smv_zmq_context = nullptr;
	}
}
