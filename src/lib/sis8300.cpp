/*****************************************************************************
 * File		  sis8300.h
 * created on 2015-07-31
 *****************************************************************************
 * Author:	D.Kalantaryan, Tel:033762/77552 kalantar
 * Email:	davit.kalantaryan@desy.de
 * Mail:	DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 *   file for ...
 ****************************************************************************/
#include "stdafx.h"
#include "sis8300.h"
#define SI5326_CONFIGURE_HEADER_NEEDED
#include "si5326_configure_header.h"


using namespace MTCA_CLASSES;

void* sis8300::mmap(void *a_addr, size_t a_length, int a_prot, int a_flags,off_t a_offset)
{
    return ::mmap(a_addr,a_length,a_prot,a_flags,m_fd,a_offset);
}

int sis8300::SetClockType(int a_type)       // a = backplaneTiming, 
{
    u_int32_t unValue, unMask(0xfffff0c0);

    unMask = 0xffffffff;
    unValue = a_type;

    device_ioc_rw rwIOC;
    rwIOC.rw_access_mode = MTCA_SET_BITS;
    rwIOC.register_size = RW_D32;
    rwIOC.barx_rw = 0;
    rwIOC.offset_rw = SIS8300_CLOCK_DISTRIBUTION_MUX_REG*4;
    rwIOC.count_rw = 1;
    rwIOC.dataPtr = (u_int64_t)(&unValue);
    rwIOC.maskPtr = (u_int64_t)(&unMask);

    return this->ioctl (PCIEDEV_SET_BITS,&rwIOC);
}

int sis8300::GetClockType()const
{
    u_int32_t unClockRegValue;
    this->read(0,SIS8300_CLOCK_DISTRIBUTION_MUX_REG*4,1,&unClockRegValue,RW_D32);
    return unClockRegValue;
}

const char* sis8300::GetClockTypeString()const
{
    u_int32_t unClockRegValue(GetClockType());
    switch (unClockRegValue)
    {
    case SIS8300_CLOCK_INTERNAL:return "internal";
    case SIS8300_BACKPLANE_CLK1:return "backplane1";
    case SIS8300_BACKPLANE_CLK2:return "backplane2";
    case SIS8300_FRONT_EXTCLCA_SMA:return "ext_sma";
    default:break;
    }
    return "Unknown";
}

typedef struct sis8300_reg_local{
    unsigned int offset; /* offset from bar0 */
    unsigned int data;   /* data which will be read / written */
}sis8300_reg_local;

static int sis8300_ReadRegister(int device, unsigned int address, unsigned int* data)
//int sis8300_ReadRegister(int device, unsigned int address, unsigned int* data)
{
    mtca::UpciedevLocal aPcieLocal = (mtca::UpciedevLocal)device;
    int nRead = aPcieLocal.read(0,address*4,1,data,RW_D32);
    if(nRead<0)throw nRead;
    return nRead;
}

static int sis8300_WriteRegister(int device, unsigned int address, unsigned int data)
{
    mtca::UpciedevLocal aPcieLocal = (mtca::UpciedevLocal)device;
    int nWrite = aPcieLocal.write(0,address*4,1,&data,RW_D32);
    if(nWrite<0)throw nWrite;
    return nWrite;
}

int sis8300::SetNumberOfSamples(int a_number)
{
    if(a_number <=0)return 0;
    a_number = (a_number%16) ? (a_number - (a_number%16) + 16 ) : a_number;
    u_int32_t unReturn = this->ioctl(SIS8300_SET_NUMBER_OF_SAMPLES,&a_number);

    //////////////////////////////////////////////////////////////////////////
    int a_fd = m_fd;
    ///
    /// \brief myReg
    ///
    const int  test_sample_block_length = a_number / 16 - 1;
    //printf("test_sample_block_length = %d[%x]\n",test_sample_block_length,test_sample_block_length);
    ///

    sis8300_reg_local myReg;
    // Get info about dual channel or single channel
    unsigned int dual_channel_memory_logic_flag, data;
    sis8300_ReadRegister(a_fd, SIS8300_FIRMWARE_OPTIONS_REG, &data);
    dual_channel_memory_logic_flag = 0 ;
    if ((data&0x4) == 0x4){dual_channel_memory_logic_flag = 1 ;}

    //
    myReg.offset = SIS8300_SAMPLE_LENGTH_REG;
    myReg.data = test_sample_block_length;
    sis8300_WriteRegister(a_fd,myReg.offset,myReg.data);
    usleep(1) ;

    int nMem = a_number/16;
    //printf("nMem = %d\n",nMem);
    if (dual_channel_memory_logic_flag == 0)  //SINGLE_CHANNEL_BUFFER_LOGIC
    {
        sis8300_WriteRegister( a_fd, SIS8300_SAMPLE_START_ADDRESS_CH1_REG + 0, 0*nMem ); // 1.  1M-Block 16 Msamples
        sis8300_WriteRegister( a_fd, SIS8300_SAMPLE_START_ADDRESS_CH1_REG + 1, 1*nMem ); // 2.  1M-Block 16 Msamples
        sis8300_WriteRegister( a_fd, SIS8300_SAMPLE_START_ADDRESS_CH1_REG + 2, 2*nMem ); // 3.  1M-Block 16 Msamples
        sis8300_WriteRegister( a_fd, SIS8300_SAMPLE_START_ADDRESS_CH1_REG + 3, 3*nMem ); // 4.  1M-Block 16 Msamples
        sis8300_WriteRegister( a_fd, SIS8300_SAMPLE_START_ADDRESS_CH1_REG + 4, 4*nMem ); // 5.  1M-Block 16 Msamples
        sis8300_WriteRegister( a_fd, SIS8300_SAMPLE_START_ADDRESS_CH1_REG + 5, 5*nMem ); // 6.  1M-Block 16 Msamples
        sis8300_WriteRegister( a_fd, SIS8300_SAMPLE_START_ADDRESS_CH1_REG + 6, 6*nMem ); // 7.  1M-Block 16 Msamples
        sis8300_WriteRegister( a_fd, SIS8300_SAMPLE_START_ADDRESS_CH1_REG + 7, 7*nMem ); // 8.  1M-Block 16 Msamples
        sis8300_WriteRegister( a_fd, SIS8300_SAMPLE_START_ADDRESS_CH1_REG + 8, 8*nMem ); // 9.  1M-Block 16 Msamples
        sis8300_WriteRegister( a_fd, SIS8300_SAMPLE_START_ADDRESS_CH1_REG + 9, 9*nMem ); // 10. 1M-Block 16 Msamples
    }
    else  //DUAL_CHANNEL_BUFFER_LOGIC
    {
        sis8300_WriteRegister( a_fd, SIS8300_SAMPLE_START_ADDRESS_CH1_REG + 0, 0*nMem ); // 1. 2M-Block 16 Msamples
        sis8300_WriteRegister( a_fd, SIS8300_SAMPLE_START_ADDRESS_CH1_REG + 1, 2*nMem ); // 2. 2M-Block 16 Msamples
        sis8300_WriteRegister( a_fd, SIS8300_SAMPLE_START_ADDRESS_CH1_REG + 2, 4*nMem ); // 3. 2M-Block 16 Msamples
        sis8300_WriteRegister( a_fd, SIS8300_SAMPLE_START_ADDRESS_CH1_REG + 3, 6*nMem ); // 4. 2M-Block 16 Msamples
        sis8300_WriteRegister( a_fd, SIS8300_SAMPLE_START_ADDRESS_CH1_REG + 4, 8*nMem ); // 5. 2M-Block 16 Msamples
    }

    return unReturn;
}

u_int32_t sis8300::GetNumberOfSamples()const
{
    return ::ioctl(m_fd,SIS8300_NUMBER_OF_SAMPLES);
}

bool sis8300::IsChannelEnable(int a_channel)const
{
    if( a_channel<0 || a_channel>=_NUMBER_OF_CHANNELS_) return false;

    u_int32_t unData;
    u_int32_t unMask(0x1);

    this->read(0,SIS8300_SAMPLE_CONTROL_REG*4,1,&unData,RW_D32);
    unMask <<= a_channel;

    return (unMask&unData) == 0;
}

void sis8300::SetChannelEnable(int a_channel,bool a_enable)
{
    if( a_channel<0 || a_channel>=_NUMBER_OF_CHANNELS_) return ;

    u_int32_t unData(a_enable ? 0x0 : 0xffffffff);
    u_int32_t unMask(0x1);

    unMask <<= a_channel;
    this->set_bits(0,SIS8300_SAMPLE_CONTROL_REG*4,1,&unData,&unMask,RW_D32);

}

int sis8300::SetOffsets()
{
    u_int32_t memoryOffset(0);
    //u_int32_t oneChannelBufferSize(GetNumberOfSamples()*2);
    u_int32_t numberOfBlocks(GetNumberOfSamples()/16);
    int nReturn(0);
    u_int32_t registerAddressOffSet( SIS8300_SAMPLE_START_ADDRESS_CH1_REG * 4);

    //printf("oneChannelBufferSize=%u\n",oneChannelBufferSize);
    //printf("numberOfBlocks=%u\n",numberOfBlocks);

    for (int i(0); i < _NUMBER_OF_CHANNELS_; ++i)
    {
        //if(IsChannelEnable(i))
        {
            if((nReturn=this->write(0,registerAddressOffSet,1,&memoryOffset,RW_D32))<0)break;
            //memoryOffset += oneChannelBufferSize;
            memoryOffset += numberOfBlocks;
            usleep(1) ;
        }

        registerAddressOffSet += 4;

    }
    return nReturn;
}

int sis8300::ConfigureADCs()
{
    u_int32_t uint_adc_mux_select;
    u_int32_t addr,data;
    //sis8300_reg myReg;
    int myReg_offset = SIS8300_ADC_SPI_REG;
    u_int32_t myReg_data;

    for(int adc_device_no(0);adc_device_no<_NUMBER_OF_ADCS_;++adc_device_no)
    {
        //if (adc_device_no > 4) {return -1;}
        uint_adc_mux_select = adc_device_no << 24 ;

        //myReg.offset = SIS8300_ADC_SPI_REG;

        // output type LVDS
        addr = (0x14 & 0xffff) << 8 ;
        data = (0x40 & 0xff)  ;
        myReg_data = uint_adc_mux_select + addr + data;
        //ioctl(device, SIS8300_REG_WRITE, &myReg);
        usleep(1) ;

        addr = (0x16 & 0xffff) << 8 ;
        data = (0x00 & 0xff)  ;
        myReg_data = uint_adc_mux_select + addr + data;
        //ioctl(device, SIS8300_REG_WRITE, &myReg);
        this->write(0,myReg_offset,1,&myReg_data,RW_D32);
        usleep(1) ;

        addr = (0x17 & 0xffff) << 8 ;
        data = (0x00 & 0xff)  ;
        myReg_data = uint_adc_mux_select + addr + data;
        //ioctl(device, SIS8300_REG_WRITE, &myReg);
        this->write(0,myReg_offset,1,&myReg_data,RW_D32);
        usleep(1) ;

        // register update cmd
        addr = (0xff & 0xffff) << 8 ;
        data = (0x01 & 0xff)  ;
        myReg_data = uint_adc_mux_select + addr + data;
        //ioctl(device, SIS8300_REG_WRITE, &myReg);
        this->write(0,myReg_offset,1,&myReg_data,RW_D32);
        usleep(1) ;
    }

    return 0;
}

int sis8300::ADC_SPI_SetupDepr()
{
    for(int adc_num(0); adc_num<_NUMBER_OF_ADCS_; ++adc_num)
        ADC_SPI_SetupPrvt(adc_num);
    return 0;
}

int sis8300::SetDmaTriggerSource(const char* a_source_name)
{
    return this->ioctl(MTCA_SET_ADC_TIMER,const_cast<char*>(a_source_name));
}

int sis8300::SetGenEventSource(const char* a_source_name)
{
    return this->ioctl(MTCA_SET_ADC_GEN_EVNT_SRC,const_cast<char*>(a_source_name));
}

int sis8300::SI5326_set_clock_multiplier(int a_coef)
{
    if (a_coef < 1 || a_coef>= s_cnMaxMultKoeficient9MHz) return -1;

    return si5326_set_external_clock_multiplier_prvt(s_cvConfigParams9MHz[a_coef].bw_sel,
                                                     s_cvConfigParams9MHz[a_coef].n1_hs,
                                                     s_cvConfigParams9MHz[a_coef].n1_clk1,s_cvConfigParams9MHz[a_coef].n1_clk2,
                                                     s_cvConfigParams9MHz[a_coef].n2_hs,s_cvConfigParams9MHz[a_coef].n2_ls,
                                                     s_cvConfigParams9MHz[a_coef].n31);
}


int sis8300::SI5326_set_clock_multiplier_250MHz(int a_coef)
{
    if (a_coef < 1 || a_coef>= s_cnMaxMultKoeficient250MHz) return -1;

    return si5326_set_external_clock_multiplier_prvt(s_cvConfigParams250MHz[a_coef].bw_sel,
                                                     s_cvConfigParams250MHz[a_coef].n1_hs,
                                                     s_cvConfigParams250MHz[a_coef].n1_clk1,s_cvConfigParams250MHz[a_coef].n1_clk2,
                                                     s_cvConfigParams250MHz[a_coef].n2_hs,s_cvConfigParams250MHz[a_coef].n2_ls,
                                                     s_cvConfigParams250MHz[a_coef].n31);
}



int sis8300::InitDevice(int a_clockType,int a_pre_trigger_delay, int a_nNumSamples,
                        int a_trigger_type, int a_nTrigger_source, u_int8_t* a_divideValues)
{
    int nRet = InitDevicePre2(a_clockType,a_pre_trigger_delay,
                             a_trigger_type,a_nTrigger_source,a_divideValues);
    // Set number of samples
    this->SetNumberOfSamples(a_nNumSamples);
    return nRet;
}

// divide=1 => bypass
int sis8300::InitDevicePre2(int a_clockType,int a_pre_trigger_delay,
                           int a_trigger_type, int a_nTrigger_source,u_int8_t* a_divideValues)
{
    sis8300_reg_local myReg;

    try
    {
        //sis8300_ReadRegister(m_fd, SIS8300_IDENTIFIER_VERSION_REG, &data);

        // Clock Distribution Multiplexer
        myReg.offset = SIS8300_CLOCK_DISTRIBUTION_MUX_REG;
        myReg.data = a_clockType;
        sis8300_WriteRegister(m_fd,myReg.offset,myReg.data);

        // ADC
        for (int i_adc(0);i_adc<5;i_adc++)
        {
            this->ADC_SPI_SetupPrvt(i_adc);
        }

        // AD9510 Clock Distributuon IC
        //ch_divider_configuration_array[i] :
        //	bits <3:0>:   Divider High
        //	bits <7:4>:   Divider Low
        //	bits <11:8>:  Phase Offset
        //	bit  <12>:    Select Start High
        //	bit  <13>:    Force
        //	bit  <14>:    Nosyn (individual)
        //	bit  <15>:    Bypass Divider
        this->AD9510_SPI_Setup(a_divideValues, 1 /*ad9510_synch_cmd*/ );

        // disable ddr2 test write interface
        myReg.offset = DDR2_ACCESS_CONTROL;
        myReg.data = 0 ; // (1<<DDR2_PCIE_TEST_ENABLE);
        sis8300_WriteRegister(m_fd,myReg.offset,myReg.data);
        usleep(1) ;

        //
        myReg.offset = SIS8300_PRETRIGGER_DELAY_REG;
        myReg.data = a_pre_trigger_delay;
        sis8300_WriteRegister(m_fd,myReg.offset,myReg.data);
        usleep(1) ;

        //
        //myReg.offset = SIS8300_SAMPLE_LENGTH_REG;
        //myReg.data = test_sample_block_length;
        //sis8300_WriteRegister(m_fd,myReg.offset,myReg.data);
        //usleep(1) ;

        // Piti nayvi
        myReg.offset = SIS8300_SAMPLE_CONTROL_REG;
        myReg.data = 0x0; // enable ch1-10
        if (a_trigger_type == 2)
        {
            myReg.data |= 0x800; // enable external trigger
        }
        else if (a_trigger_type == 1) // interal trigger
        {
            myReg.data |= 0x400; // enable internal trigger
        }
        sis8300_WriteRegister(m_fd,myReg.offset,myReg.data);
        usleep(1) ;

        //
        if (a_trigger_type == 2) // external trigger setup
        {
            switch(a_nTrigger_source)
            {
            case TRG_SRC_HARLINK_IN_OUT:
                myReg.offset = SIS8300_HARLINK_IN_OUT_CONTROL_REG ;
                myReg.data = 0x100  ; // Enable Harlink 1 Input
                sis8300_WriteRegister(m_fd,myReg.offset,myReg.data);
                break;
            case TRG_SRC_BACKPLANE_RX18:
                myReg.offset = SIS8300_L_MLVDS_IO_CONTROL_REG ;
                myReg.data = 0x400  ; // Enable ... Input
                sis8300_WriteRegister(m_fd,myReg.offset,myReg.data);
                break;
            default: break;
            }
        }
        else if (a_trigger_type == 1) // internal trigger setup
        {
            myReg.data = 0x0  ; //
            myReg.data = myReg.data + 0x7000000  ; // Enable / GT / FIR
            //  myReg.data = myReg.data + 0x6000000  ; // Enable / GT /
            myReg.data = myReg.data + 0x100000  ; //Pulse Length = 0x10
            myReg.data = myReg.data + 0x0f0F  ; // G = 15, P = 14

            for (int var(0); var < 10; ++var) {


                myReg.offset = SIS8300_TRIGGER_SETUP_CH1_REG + var;
                sis8300_WriteRegister(m_fd,myReg.offset,myReg.data);
            }
            //myReg.data = 0x1000110  ; // Threshold Off / On
            myReg.data = 0x500  ; // FIR Threshold  On
            //myReg.data = 32600  ; // FIR Threshold  On

            for (int var(0); var < 10; ++var) {
                myReg.offset = SIS8300_TRIGGER_THRESHOLD_CH1_REG + var;
                sis8300_WriteRegister(m_fd,myReg.offset,myReg.data);
            }

            // reset sample logic
            sis8300_WriteRegister( m_fd, SIS8300_ACQUISITION_CONTROL_STATUS_REG, 0x00004 ); //

        }

    }
    catch(int a_error)
    {
        return a_error;
    }
    catch(...)
    {
        return -1;
    }

    return 0;

}


int sis8300::SetDivders(u_int8_t* a_divideValues)
{
    try
    {
        // AD9510 Clock Distributuon IC
        //ch_divider_configuration_array[i] :
        //	bits <3:0>:   Divider High
        //	bits <7:4>:   Divider Low
        //	bits <11:8>:  Phase Offset
        //	bit  <12>:    Select Start High
        //	bit  <13>:    Force
        //	bit  <14>:    Nosyn (individual)
        //	bit  <15>:    Bypass Divider
        this->AD9510_SPI_Setup(a_divideValues, 1 /*ad9510_synch_cmd*/ );

        // disable ddr2 test write interface
    }
    catch(int a_error)
    {
        return a_error;
    }
    catch(...)
    {
        return -1;
    }

    return 0;

}



/******************************************************************************/
/*                                                                            */
/*     SIS8300_ADC_SPI_Setup                                                  */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* input:                                                                     */
/* adc_device_no: 0 to 4                                                      */
/*                                                                            */
/* return_codes:	                                                      */
/* 0:   	OK		                                              */
/* 0x800    FSM is BUSY                                                       */
/*                                                                            */
/******************************************************************************/
int sis8300::ADC_SPI_SetupPrvt(unsigned int a_adc_device_no)
{
    int a_fd = m_fd;
    ////////////////////////////////////////////
    unsigned int uint_adc_mux_select;
    unsigned int addr,data;
    sis8300_reg_local myReg;

    if (a_adc_device_no > 4) {return -1;}
    uint_adc_mux_select = a_adc_device_no << 24 ;

    myReg.offset = SIS8300_ADC_SPI_REG;

    // output type LVDS
    addr = (0x14 & 0xffff) << 8 ;
    data = (0x40 & 0xff)  ;
    myReg.data = uint_adc_mux_select + addr + data;
    sis8300_WriteRegister(a_fd,myReg.offset,myReg.data);
    usleep(1) ;

    addr = (0x16 & 0xffff) << 8 ;
    data = (0x00 & 0xff)  ;
    myReg.data = uint_adc_mux_select + addr + data;
    sis8300_WriteRegister(a_fd,myReg.offset,myReg.data);
    usleep(1) ;

    addr = (0x17 & 0xffff) << 8 ;
    data = (0x00 & 0xff)  ;
    myReg.data = uint_adc_mux_select + addr + data;
    sis8300_WriteRegister(a_fd,myReg.offset,myReg.data);
    usleep(1) ;

    // register update cmd
    addr = (0xff & 0xffff) << 8 ;
    data = (0x01 & 0xff)  ;
    myReg.data = uint_adc_mux_select + addr + data;
    sis8300_WriteRegister(a_fd,myReg.offset,myReg.data);
    usleep(1) ;

    return 0;
}



/****************************************************************************************************************************************/
/****************************************************************************************************************************************/
/****************************************************************************************************************************************/

#define _DIV_TO_WRITE_(_a_divider) \
    ((((_a_divider)%_DIVIDER_MAX_VALUE_) /2 - 1)<<4 | \
       (((_a_divider)%_DIVIDER_MAX_VALUE_) - (((_a_divider)%_DIVIDER_MAX_VALUE_) /2 - 1) - 2))

    // Out 4  (FPGA DIV-CLK05) : LVDS 3.5 mA
    // Out 5  (ADC3-CLK, ch5/6) : LVDS 3.5 mA
    // Out 6  (ADC2-CLK, ch3/4) : LVDS 3.5 mA
    // Out 7  (ADC1-CLK, ch1/2) : LVDS 3.5 mA

    // Out 4  (FPGA DIV-CLK69) : LVDS 3.5 mA
    // Out 5  (ADC5-CLK, ch9/10) : LVDS 3.5 mA
    // Out 6  (ADC4-CLK, ch7/8) : LVDS 3.5 mA
    // Out 7  (Frontpanel Clk, Harlink) : LVDS 3.5 mA

//ch_divider_configuration_array[i] :
//	bits <3:0>:   Divider High
//	bits <7:4>:   Divider Low
//	bits <11:8>:  Phase Offset
//	bit  <12>:    Select Start High
//	bit  <13>:    Force
//	bit  <14>:    Nosyn (individual)
//	bit  <15>:    Bypass Divider
//
//	i=0:	AD910 #1 Out 7  (ADC1-CLK, ch1/2)
//	i=1:	AD910 #1 Out 6  (ADC2-CLK, ch3/4)
//	i=2:	AD910 #1 Out 5  (ADC3-CLK, ch5/6)
//	i=3:	AD910 #2 Out 6  (ADC4-CLK, ch7/8)
//	i=4:	AD910 #2 Out 5  (ADC5-CLK, ch9/10)
//	i=5:	AD910 #2 Out 7  (Frontpanel Clk, Harlink)
//	i=6:	AD910 #1 Out 4  (FPGA DIV-CLK05) used for synch. of external Triggers
//	i=7:	AD910 #2 Out 4  (FPGA DIV-CLK69) used for sychn. of AD910 ISc

#define writeIOCTL(_a_ignored_,_a_register_,_a_data_) write(0,(_a_register_)*4,1,&(_a_data_),RW_D32)

int sis8300::AD9510_SPI_Setup(u_int8_t* a_dividerValues, unsigned int a_ad9510_synch_cmd )
//int sis8300::AD9510_SPI_Setup( unsigned int* ch_divider_configuration_array, unsigned int ad9510_synch_cmd)
{
    int32_t      wr_data;
    unsigned int ch_divider_configuration_array[10];
    unsigned int ad9510_synch_cmd = a_ad9510_synch_cmd;
    sis8300& sis8300dev_ = *this;

    for(int i(0); i<5; ++i)
    {
        ch_divider_configuration_array[i] = 0x0000 + _DIV_TO_WRITE_(a_dividerValues[i]) ;  // (ADCX-CLK, chk/k+1)
    }

    ch_divider_configuration_array[5] = 0x0000 + 0x11 ;  // (Frontpanel Clk, Harlink)
    ch_divider_configuration_array[6] = 0x0000 + 0x11 ;  // (FPGA DIV-CLK05) used for synch. of external Triggers
    ch_divider_configuration_array[7] = 0x0000 + 0x11 ;  // (FPGA DIV-CLK05) used for synch. of external Triggers
    ch_divider_configuration_array[8] = 0;
    ch_divider_configuration_array[9] = 0;

    // set AD9510 to Bidirectional Mode and Soft Reset
    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x00B0;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    // AD9510 (ADC channels 1 to 6)
    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x0090;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x0A01;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x3C0B;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x3D0B;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x3E0B;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x3F0B;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x4002;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x4102;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x4202;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x4302;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x4500;
    //rw_Ctrl.data = myReg.data + 0x1D ;
    wr_data = wr_data + 0x10 ; // Power-Down RefIn
    wr_data = wr_data + 0x08 ; // Shut Down Clk to PLL Prescaler
    switch (ch_divider_configuration_array[8]){
        case 0:
            wr_data = wr_data + 0x04 ; // Power-Down CLK2
            wr_data = wr_data + 0x01 ; // CLK1 Drives Distribution Section
            break;
        case 1:
            wr_data = wr_data + 0x02 ; // Power-Down CLK2
            wr_data = wr_data + 0x00 ; // CLK1 Drives Distribution Section
            break;
        default:
            wr_data = wr_data + 0x04 ; // Power-Down CLK2
            wr_data = wr_data + 0x01 ; // CLK1 Drives Distribution Section
            break;
    }
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x5000;
    wr_data = wr_data + (ch_divider_configuration_array[6] & 0xff);
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x5100;
    wr_data = wr_data + ((ch_divider_configuration_array[6] >> 8) & 0xff);
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x5200;
    wr_data = wr_data + (ch_divider_configuration_array[2] & 0xff);
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x5300;
    wr_data = wr_data + ((ch_divider_configuration_array[2] >> 8) & 0xff);
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x5400;
    wr_data = wr_data + (ch_divider_configuration_array[1] & 0xff);
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data = AD9510_GENERATE_SPI_RW_CMD + 0x5500;
    wr_data = wr_data + ((ch_divider_configuration_array[1] >> 8) & 0xff);
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x5600;
    wr_data = wr_data + (ch_divider_configuration_array[0] & 0xff);
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x5700;
    wr_data = wr_data + ((ch_divider_configuration_array[0] >> 8) & 0xff);
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    // update command
    wr_data    = AD9510_GENERATE_SPI_RW_CMD + 0x5A01;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    /**********Second CHIP************/
    // set AD9510 to Bidirectional Mode and Soft Reset
    wr_data    = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x00B0;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    // AD9510 (ADC channels 7 to 10)
    wr_data    = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x0090;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x0A01;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x3c0B;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x3D0B;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x3E0B;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x3F0B;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x4002;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x4102;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x4202;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x4302;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x4500;
    wr_data = wr_data + 0x10 ; // Power-Down RefIn
    wr_data = wr_data + 0x08 ; // Shut Down Clk to PLL Prescaler
    switch (ch_divider_configuration_array[9]){
        case 0:
            wr_data = wr_data + 0x04 ; // Power-Down CLK2
            wr_data = wr_data + 0x01 ; // CLK1 Drives Distribution Section
            break;
        case 1:
            wr_data = wr_data + 0x02 ; // Power-Down CLK2
            wr_data = wr_data + 0x00 ; // CLK1 Drives Distribution Section
            break;
        default:
            wr_data = wr_data + 0x04 ; // Power-Down CLK2
            wr_data = wr_data + 0x01 ; // CLK1 Drives Distribution Section
            break;
    }
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x5000;
    wr_data = wr_data + (ch_divider_configuration_array[7] & 0xff);
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x5100;
    wr_data = wr_data + ((ch_divider_configuration_array[7] >> 8) & 0xff);
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x5200;
    wr_data = wr_data + (ch_divider_configuration_array[4] & 0xff);
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x5300;
    wr_data = wr_data + ((ch_divider_configuration_array[4] >> 8) & 0xff);
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x5400;
    wr_data = wr_data + (ch_divider_configuration_array[3] & 0xff);
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x5500;
    wr_data = wr_data + ((ch_divider_configuration_array[3] >> 8) & 0xff);
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x5600;
    wr_data = wr_data + (ch_divider_configuration_array[5] & 0xff);
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    wr_data    = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x5700;
    wr_data = wr_data + ((ch_divider_configuration_array[5] >> 8) & 0xff);
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    // update command
    wr_data    = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x5A01;
    sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
    usleep(1) ;

    // synch Cmd
    if (ad9510_synch_cmd == 1) {
        // set Function of "Function pin" to SYNCB (Default Reset)
        wr_data =  AD9510_GENERATE_SPI_RW_CMD + 0x5822;
            sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
        usleep(1) ;
        // update command
        wr_data =   AD9510_GENERATE_SPI_RW_CMD + 0x5A01;
            sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
        usleep(1) ;


        // set Function of "Function pin" to SYNCB (Default Reset)
        wr_data = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x5822;
            sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
        usleep(1) ;
        // update command
        wr_data = AD9510_GENERATE_SPI_RW_CMD + AD9510_SPI_SELECT_NO2 + 0x5A01;
            sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
        usleep(1) ;

        // set use "FPGA DIV-CLK69" for Sychn Pulse  - Logic (VIRTEX5)
        wr_data = AD9510_SPI_SET_FUNCTION_SYNCH_FPGA_CLK69  ; //set clock
            sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
        usleep(1) ;

        // generate sych Pulse   (pulse Function pins) and hold FPGA_CLK69 to sycnh pulse
        wr_data = AD9510_GENERATE_FUNCTION_PULSE_CMD + AD9510_SPI_SET_FUNCTION_SYNCH_FPGA_CLK69   ; //
            sis8300dev_.writeIOCTL( SIS8300_SPI_INTRFC, SIS8300_AD9510_SPI_REG, wr_data);
        usleep(1) ;
    }

    return 0;
}


/******************************************************************************/
/*                                                                            */
/*     SIS8300 SI526 Clock Multiplier routines                                */
/*                                                                            */
/******************************************************************************/
/*                                                                            */
/* input:                                                                     */
/*                                                                            */
/******************************************************************************/

#define SI5326_SPI_POLL_COUNTER_MAX							1000
#define SI5326_SPI_CALIBRATION_READY_POLL_COUNTER_MAX		10000

#define SIS8300_Write_Register(_a_fd_,_a_register_,_a_data_) \
    ((mtca::UpciedevLocal)(_a_fd_)).write(0,(_a_register_)*4,1,&(_a_data_),RW_D32)
#define SIS8300_Read_Register(_a_fd_,_a_register_,_a_data_ptr_) \
    ((mtca::UpciedevLocal)(_a_fd_)).read(0,(_a_register_)*4,1,(_a_data_ptr_),RW_D32)

int sis8300::si5326_clk_muliplier_write(unsigned int a_addr, unsigned int a_data)
{
    unsigned int write_data, read_data ;
    unsigned int poll_counter   ;

    // write address
    write_data = 0x0000 + (a_addr & 0xff) ; // write ADDR Instruction + register addr
    SIS8300_Write_Register(m_fd,  SIS8300_CLOCK_MULTIPLIER_SPI_REG,  write_data) ;
    usleep(10) ;

    poll_counter = 0 ;
    do {
        poll_counter++;
        SIS8300_Read_Register(m_fd,  SIS8300_CLOCK_MULTIPLIER_SPI_REG, &read_data) ;
    } while (((read_data & 0x80000000) == 0x80000000) && (poll_counter < SI5326_SPI_POLL_COUNTER_MAX)) ;
    if (poll_counter == SI5326_SPI_POLL_COUNTER_MAX) {	return -2 ;	}
    usleep(10) ;

// write data
    write_data = 0x4000 + (a_data & 0xff) ; // write Instruction + data
    SIS8300_Write_Register(m_fd,  SIS8300_CLOCK_MULTIPLIER_SPI_REG,  write_data) ;
    usleep(10) ;

    poll_counter = 0 ;
    do {
        poll_counter++;
        SIS8300_Read_Register(m_fd,  SIS8300_CLOCK_MULTIPLIER_SPI_REG, &read_data) ;
    } while (((read_data & 0x80000000) == 0x80000000) && (poll_counter < SI5326_SPI_POLL_COUNTER_MAX)) ;
    if (poll_counter == SI5326_SPI_POLL_COUNTER_MAX) {	return -2 ;	}

    return 0 ;
}


int sis8300::si5326_clk_muliplier_read(unsigned int a_addr, unsigned int *a_data)
{
    unsigned int write_data, read_data ;
    unsigned int poll_counter   ;

    // read address
    write_data = 0x0000 + (a_addr & 0xff) ; // read ADDR Instruction + register addr
    SIS8300_Write_Register(m_fd,  SIS8300_CLOCK_MULTIPLIER_SPI_REG,  write_data) ;
    usleep(10) ;

    poll_counter = 0 ;
    do {
        poll_counter++;
        SIS8300_Read_Register(m_fd,  SIS8300_CLOCK_MULTIPLIER_SPI_REG, &read_data) ;
    } while (((read_data & 0x80000000) == 0x80000000) && (poll_counter < SI5326_SPI_POLL_COUNTER_MAX)) ;
    if (poll_counter == SI5326_SPI_POLL_COUNTER_MAX) {	return -2 ;	}
    usleep(10) ;

    // read data
    write_data = 0x8000  ; // read Instruction + data
    SIS8300_Write_Register(m_fd,  SIS8300_CLOCK_MULTIPLIER_SPI_REG,  write_data) ;
    usleep(10) ;

    poll_counter = 0 ;
    do {
        poll_counter++;
        SIS8300_Read_Register(m_fd,  SIS8300_CLOCK_MULTIPLIER_SPI_REG, &read_data) ;
    } while (((read_data & 0x80000000) == 0x80000000) && (poll_counter < SI5326_SPI_POLL_COUNTER_MAX)) ;
    if (poll_counter == SI5326_SPI_POLL_COUNTER_MAX) {	return -2 ;	}
    *a_data = (read_data ) ;
    return (0) ;
}



int sis8300::si5326_clk_muliplier_internal_calibration_cmd()
{
    unsigned int write_data, read_data ;
    unsigned int poll_counter, cal_poll_counter  ;
    // write address
    write_data = 0x0000 + 136 ; // write ADDR Instruction + register addr
    SIS8300_Write_Register(m_fd,  SIS8300_CLOCK_MULTIPLIER_SPI_REG,  write_data) ;
    usleep(10) ;

    poll_counter = 0 ;
    do {
        poll_counter++;
        SIS8300_Read_Register(m_fd,  SIS8300_CLOCK_MULTIPLIER_SPI_REG, &read_data) ;
    } while (((read_data & 0x80000000) == 0x80000000) && (poll_counter < SI5326_SPI_POLL_COUNTER_MAX)) ;
    if (poll_counter == SI5326_SPI_POLL_COUNTER_MAX) {	return -2 ;	}
    usleep(10) ;

    // write data
    write_data = 0x4000 + 0x40 ; // write Instruction + data
    SIS8300_Write_Register(m_fd,  SIS8300_CLOCK_MULTIPLIER_SPI_REG,  write_data) ;
    usleep(10) ;

    poll_counter = 0 ;
    do {
        poll_counter++;
        SIS8300_Read_Register(m_fd,  SIS8300_CLOCK_MULTIPLIER_SPI_REG, &read_data) ;
    } while (((read_data & 0x80000000) == 0x80000000) && (poll_counter < SI5326_SPI_POLL_COUNTER_MAX)) ;
    if (poll_counter == SI5326_SPI_POLL_COUNTER_MAX) {	return -2 ;	}
    usleep(10) ;

    // poll until Calibration is ready
    cal_poll_counter = 0 ;
    do {
        cal_poll_counter++;
        // read data
        write_data = 0x8000  ; // read Instruction + data
        SIS8300_Write_Register(m_fd,  SIS8300_CLOCK_MULTIPLIER_SPI_REG,  write_data) ;
        usleep(10) ;

        poll_counter = 0 ;
        do {
            poll_counter++;
            SIS8300_Read_Register(m_fd,  SIS8300_CLOCK_MULTIPLIER_SPI_REG, &read_data) ;
        } while (((read_data & 0x80000000) == 0x80000000) && (poll_counter < SI5326_SPI_POLL_COUNTER_MAX)) ;

        if (poll_counter == SI5326_SPI_POLL_COUNTER_MAX) {	return -2 ;	}
        usleep(10) ;

    } while (((read_data & 0x40) == 0x40) && (cal_poll_counter < SI5326_SPI_CALIBRATION_READY_POLL_COUNTER_MAX)) ;
    if (cal_poll_counter == SI5326_SPI_CALIBRATION_READY_POLL_COUNTER_MAX) {	return -3 ;	}

    return 0 ;
}


int sis8300::si5326_get_status_external_clock_multiplier(unsigned int *a_status_ptr  )
{
    int rc;
    unsigned int data ;
    rc = si5326_clk_muliplier_read(128, &data); //
    *a_status_ptr = data & 0x1 ;
    rc = si5326_clk_muliplier_read( 129, &data); //
    *a_status_ptr = *a_status_ptr + (data & 0x2) ;
    return rc ;
}



int sis8300::si5326_bypass_external_clock_multiplier()
{
    int rc;
    rc = si5326_clk_muliplier_write(21, 0xFE); // override clksel_pin
    rc = si5326_clk_muliplier_write(0, 0x2); // Bypass
    rc = si5326_clk_muliplier_write(3, 0x0); // select clkin1
    rc = si5326_clk_muliplier_write(11, 0x0); //  PowerDown clk2
    return rc ;
}


int sis8300::si5326_bypass_internal_ref_osc_multiplier()
{
    int rc;
    rc = si5326_clk_muliplier_write(21, 0xFE); // override clksel_pin
    rc = si5326_clk_muliplier_write(0, 0x42); // Bypass + free run
    rc = si5326_clk_muliplier_write(3, 0x40); // select clkin2
    rc = si5326_clk_muliplier_write(11, 0x0); //  PowerDown clk1
    return rc ;
}



int sis8300::si5326_set_external_clock_multiplier_prvt(unsigned int a_bw_sel,
                                                       unsigned int a_n1_hs,
                                                       unsigned int a_n1_clk1, unsigned int a_n1_clk2,
                                                       unsigned int a_n2_hs, unsigned int a_n2_ls,
                                                       unsigned int a_n31 )
{
    volatile unsigned int n1_clk1_val = a_n1_clk1 - 1;
    volatile unsigned int n1_clk2_val = a_n1_clk2 - 1;
    volatile unsigned int n1_hs_val   = a_n1_hs - SHIFT_FOR_N1_HS ;
    volatile unsigned int n2_hs_val   = a_n2_hs - SHIFT_FOR_N2_HS;
    volatile unsigned int n2_ls_val   = a_n2_ls - SHIFT_FOR_N2_LS;
    volatile unsigned int n31_val     = a_n31 - SHIFT_FOR_N31;
    volatile unsigned int reg40val(0);


    si5326_clk_muliplier_write(0, 0x0);   // No Bypass
    si5326_clk_muliplier_write(11, 0x02); //  PowerDown clk2

    // N3 = 1
    si5326_clk_muliplier_write(43, ((n31_val >> 16) & 0x7) );      //  N31 bits 18:16
    si5326_clk_muliplier_write(44, ((n31_val >> 8) & 0xff));       //  N31 bits 15:8
    si5326_clk_muliplier_write(45, (n31_val  & 0xff) );            //  N31 bits 7:0

    reg40val = ((n2_hs_val & 0x7) << 5) | (a_n2_ls>>16);
    si5326_clk_muliplier_write(40, reg40val);					   //  N2_LS bits 19:16  and N2_HS [2:0]
    si5326_clk_muliplier_write(41, ((n2_ls_val >> 8) & 0xff));     //  N2_LS bits 15:8
    si5326_clk_muliplier_write(42, (n2_ls_val  & 0xff));		   //  N2_LS bits 7:0

    si5326_clk_muliplier_write(25, (n1_hs_val << 5)); //

    si5326_clk_muliplier_write(31, ((n1_clk1_val >> 16) & 0xf));   //  NC1_LS (low speed divider) bits 19:16
    si5326_clk_muliplier_write(32, ((n1_clk1_val >> 8) & 0xff));   //  NC1_LS (low speed divider) bits 15:8
    si5326_clk_muliplier_write(33, (n1_clk1_val  & 0xff) );        //  NC1_LS (low speed divider) bits 7:0

    si5326_clk_muliplier_write(34, ((n1_clk2_val >> 16) & 0xf));   //  NC2_LS (low speed divider) bits 19:16
    si5326_clk_muliplier_write(35, ((n1_clk2_val >> 8) & 0xff));   //  NC2_LS (low speed divider) bits 15:8
    si5326_clk_muliplier_write(36, (n1_clk2_val  & 0xff));         //  NC2_LS (low speed divider) bits 7:0


    si5326_clk_muliplier_write(2, (a_bw_sel << 5) );               //BWSEL_REG

    si5326_clk_muliplier_internal_calibration_cmd();

    return 0;
}
