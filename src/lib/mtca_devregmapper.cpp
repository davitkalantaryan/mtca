/*****************************************************************************
 * File		  mtca_upciedevlocal.cpp
 * created on 2017-05-20
 *****************************************************************************
 * Author:	D.Kalantaryan, Tel:033762/77552 kalantar
 * Email:	davit.kalantaryan@desy.de
 * Mail:	DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 *   file for ...
 ****************************************************************************/
//#include "stdafx.h"
#include "mtca_devregmapper.hpp"

#include <memory.h>
#include <mtsys/mtcagen_io.h>

#ifdef WIN32
#include <WINDOWS.H>
#include <io.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <share.h>
//#define open_cm    _open
#define close_cm   _close
#define read_cm    _read
#define write_cm   _write
static inline int open_cm(const char* a_filename,int a_flags,int a_mode=_S_IREAD|_S_IWRITE ){int nFD;_sopen_s(&nFD,a_filename,a_flags,_SH_DENYNO,a_mode);return nFD;}
/// Piti nayvi
#define __off_t long
static int pread(int a_fd,void* a_buf,size_t a_count, __off_t a_offset){_lseek(a_fd,a_offset,SEEK_SET);return read_cm(a_fd,a_buf,a_count);}
static int pwrite(int a_fd,const void* a_buf,size_t a_count, __off_t a_offset){_lseek(a_fd,a_offset,SEEK_SET);return write_cm(a_fd,a_buf,a_count);}
#else
#include <sys/mman.h>
#define open_cm    open
#define close_cm   close
#define read_cm    read
#define write_cm   write
#endif

#include <stdio.h>
#include <string.h>

#ifndef PAGE_SIZE
#define PAGE_SIZE       4096
#endif
//#define MAP_FILE_SIZE   4096 // One page
#define FILE_ONLY(__full_path__) (strrchr((__full_path__),'/') ? (strrchr((__full_path__),'/')+1) : (__full_path__))
#define __DEBUG__(...) do{printf("fl:%s;ln:%d; => ",FILE_ONLY(__FILE__),__LINE__);printf(__VA_ARGS__);printf("\n");}while(0)


mtca::DevRegMapper::DevRegMapper()
    :   m_fd(-1), m_pBase(MAP_FAILED)
{
}


mtca::DevRegMapper::~DevRegMapper()
{
    closeC();
}


void* mtca::DevRegMapper::openAndMapC(const char* a_devName,int a_nBar, int a_flags)
{
    closeC();
    m_fd = ::open(a_devName,a_flags);

    __DEBUG__("m_fd=%d, bar=%d",m_fd,a_nBar);
    if(m_fd<0){
        // ser error message
        return MAP_FAILED;
    }
    m_pBase = ::mmap(NULL,(a_nBar+1)*PAGE_SIZE,PROT_READ|PROT_WRITE,MAP_SHARED,m_fd,0);
    __DEBUG__("mmapRes=%p\n",m_pBase);

    return m_pBase;
}

void mtca::DevRegMapper::closeC()
{
    if(m_fd>0){
        ::munmap(m_pBase,PAGE_SIZE);
        m_pBase = MAP_FAILED;
        ::close(m_fd);
        m_fd = -1;
    }
}
