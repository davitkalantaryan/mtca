/*
 *	File: mtcagen_local.cpp
 *
 *	Created on: Apr 20, 2016
 *	Authors:
 *      Davit Kalantaryan (Email: davit.kalantaryan@desy.de)
 *
 *
 */

#define TIME_TO_SLEEP_IN_SEC    2

#include "stdafx.h"
#include "mtcagen_local.h"
#include <stddef.h>
#include <time.h>
#include <memory.h>
#include <stdio.h>
#include <stdarg.h>
//#include <varargs.h>

using namespace MTCA_CLASSES;

#if 0
pthread_t                       m_UpdateThread;
volatile int                    m_bUpdate;
std::vector                     m_Entries;
ADAV_CLASSES::lightweight_mutex m_mutex;
#endif


mtcagen_local::mtcagen_local(int a_fd)
    :   mtca::UpciedevLocal(a_fd),
        m_UpdateThread(),
        m_bUpdate(1),
        m_Entries(),
        m_mutex()
{
    pthread_create( &m_UpdateThread,  NULL, ThreadFuncUpdate, this );
}


mtcagen_local::mtcagen_local(const char* a_devName,int a_flag)
    :   mtca::UpciedevLocal(a_devName,a_flag)
{
}


mtcagen_local::~mtcagen_local()
{
    size_t unSize = m_buffers.size();
    m_bUpdate = 0;
    pthread_join( m_UpdateThread, NULL );

    for(size_t i(0); i<unSize;++i)
    {
        free(m_buffers[i]);
    }
}


int mtcagen_local::register_for_changes_st(void* a_callback_data,TYPE_UPDATE a_update_func,
                                           int a_bar,int a_first_reg_address,int a_num_of_regs)
{
    int nRegSize = 1<<GetRegisterSizeMode();
    int size_of_regs_in_bytes = nRegSize*a_num_of_regs;
    SSingleEntry aSingleEntry;
    size_t unSize,i(0);
    bool bFound(false);
    unsigned char* pcuBuffer = NULL;

    if(size_of_regs_in_bytes>INTERESTED_AREA_MAX_SIZE)
    {
        size_of_regs_in_bytes = INTERESTED_AREA_MAX_SIZE;
        a_num_of_regs = INTERESTED_AREA_MAX_SIZE / nRegSize;
    }

    pcuBuffer = (unsigned char*)malloc(size_of_regs_in_bytes);
    if(!pcuBuffer) {return -1;}

#if 0
    typedef struct SSingleEntry
    {
        TYPE_UPDATE callback;
        void*       callback_data;
        int         key;
        int         bar;
        int         first_reg_address;
        int         number_of_interested_regs;
        int         size_of_interested_regs_in_bytes;
    }SSingleEntry;
#endif

    //printf("pFunc=%p\n",a_update_func);

    //aSingleEntry.m_callback = (void (*)(void*clb_data,ARGUMENT_LIST))pFunc;
    aSingleEntry.m_callback = a_update_func;
    aSingleEntry.callback_data = a_callback_data;
    //aSingleEntry.key = ?;
    aSingleEntry.bar = a_bar;
    aSingleEntry.first_reg_address = a_first_reg_address;
    aSingleEntry.number_of_interested_regs = a_num_of_regs;
    aSingleEntry.size_of_interested_regs_in_bytes  = size_of_regs_in_bytes;

    m_mutex.lock();
    unSize = m_Entries.size();

    for(;i<unSize;++i)
    {
        if(m_Entries[i].key<0)
        {
            bFound = true;
            aSingleEntry.key = i;
            m_Entries[i] = aSingleEntry;
            m_buffers[i] = pcuBuffer;
            break;
        }
    }

    if(!bFound)
    {
        aSingleEntry.key = unSize;
        m_Entries.push_back(aSingleEntry);
        m_buffers.push_back(pcuBuffer);
    }


    m_mutex.unlock();

    return aSingleEntry.key;
}


void* mtcagen_local::ThreadFuncUpdate(void* a_this)
{
    return ((mtcagen_local*)a_this)->ThreadFuncUpdate();
}


#define DEBUG_RETURN
#define USE_SIMPLE_MEMCPY


void* mtcagen_local::ThreadFuncUpdate()
{
    SSingleEntry* pEnt;
    size_t i,unNumOfClients;
    const struct timespec aTimeToSleep = {TIME_TO_SLEEP_IN_SEC,0};
    siginfo_t aSigInfo;

    unsigned char* pcDataFromDev = (unsigned char*)((void*)(aSigInfo._sifields._pad + OFFSET_TO_REGISTER_DATA));

#ifdef DEBUG_RETURN
    int i2;
    unsigned int* pnDataFrmSrc = (unsigned int*)((void*)pcDataFromDev);
    unsigned int* pnOldData;
#endif

#ifndef USE_SIMPLE_MEMCPY
    bool bModified;
    unsigned char *pcFirstRegAddress, *pcCurRegAddress;
    int nNumOfRegs;

    int nRegSize = 1<<GetRegisterSizeMode();
    int i3;
#endif

    while (m_bUpdate)
    {
        nanosleep(&aTimeToSleep,NULL);

        if(m_fd<0){continue;}

        m_mutex.lock();
        unNumOfClients = m_Entries.size();
        for(i = 0; i<unNumOfClients;++i)
        {
            pEnt = &m_Entries[i];
            //upciedev_local::read( pEnt->bar,pEnt->first_reg_address,pEnt->number_of_interested_regs,pcDataFromDev);

#ifdef DEBUG_RETURN
            pnOldData = (unsigned int*)((void*)m_buffers[i]);
            for(i2=0;i2<pEnt->number_of_interested_regs;++i2)
            {
                printf("old[%d]=%u, ",i2,pnOldData[i2]);
            }
            printf("\n");
            for(i2=0;i2<pEnt->number_of_interested_regs;++i2)
            {
                printf("new[%d]=%u, ",i2,pnDataFrmSrc[i2]);
            }
            printf("\n\n");
#endif

#ifdef USE_SIMPLE_MEMCPY

            mtca::UpciedevLocal::read( pEnt->bar,pEnt->first_reg_address,pEnt->number_of_interested_regs,pcDataFromDev);
            if(memcmp(pcDataFromDev,m_buffers[i],pEnt->size_of_interested_regs_in_bytes))

#else   // #ifdef USE_SIMPLE_MEMCPY

            pcCurRegAddress = pcDataFromDev;
            for(i3=0;i3<pEnt->number_of_interested_regs;++i3)
            {
                upciedev_local::read( pEnt->bar,pEnt->first_reg_address+i3*nRegSize,1,pcCurRegAddress);
            }
#endif  // #ifdef USE_SIMPLE_MEMCPY
            {
                (*(pEnt->m_callback))(pEnt->callback_data,
                                    pEnt->bar,pEnt->first_reg_address,pEnt->number_of_interested_regs,pcDataFromDev);
                memcpy(m_buffers[i],pcDataFromDev,pEnt->size_of_interested_regs_in_bytes);
            }
        }
        m_mutex.unlock();
    }

    return NULL;
}



void mtcagen_local::delete_entry(int a_key)
{
    int nSize,i(a_key);
    bool bIsLast(true);
    unsigned char* pcBuffer = NULL;

    if(a_key<0) return;

    m_mutex.lock();
    nSize = m_Entries.size();
    if(a_key>=nSize)
    {
        m_mutex.unlock();
        return;
    }

    pcBuffer = m_buffers[a_key];
    m_buffers[a_key] = NULL;
    m_Entries[a_key].key = -1;

    for(;i<nSize;++i)
    {
        if(m_Entries[i].key>=0)
        {
            bIsLast = false;
            break;
        }
    }

    if(bIsLast)
    {
        m_Entries.erase(m_Entries.begin()+a_key,m_Entries.begin()+(nSize-1));
    }

    m_mutex.unlock();

    free(pcBuffer);
}


void* mtcagen_local::GetFunctionPointer(int a_first, ...)
{
    void* pFunc;
    va_list arg_list;
    va_start( arg_list, a_first );
    pFunc = va_arg(arg_list,void*);
    va_end(arg_list);
    printf("pFunc=%p\n",pFunc);
    return pFunc;
}
