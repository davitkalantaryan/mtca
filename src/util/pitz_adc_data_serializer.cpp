/*****************************************************************************
 * File			: pitz_adc_data_serializer.cpp
 * Created on	: 2017 Jun 15
 *****************************************************************************
 * Created by	: D.Kalantaryan, Tel:+49(0)33762/77552 kalantar
 * Email		: davit.kalantaryan@desy.de
 * Mail			: DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 *   This is the source file for 
 *   the class 'pitz::adc::data::Serializer'
 ****************************************************************************/

#include "pitz_adc_data_serializer.hpp"
#include <malloc.h>

#define __VERSION__	1

//namespace pizt{namespace adc{namespace data{ 
	namespace OFFSETS{
	enum {_SERIALIZER_VERSION_=0,_SPECTRUM_TYPE_,_EVENT_NUMBER_=4,_SECONDS_=8,_MCR_SECONDS_=12};
}
//}}}

pitz::adc::data::Serializer::Serializer()
	:
	m_pBuffer((uint8_ttt*)malloc(HEADER_SIZE))
{
	if(!m_pBuffer){throw "Low memory!";}
}


pitz::adc::data::Serializer::~Serializer()
{
	free(m_pBuffer);
}



uint32_ttt pitz::adc::data::Serializer::serializerVersion()const
{
	return UINT_VALUE_IN_THE_OFFSET(OFFSETS::_SERIALIZER_VERSION_);
}


uint32_ttt pitz::adc::data::Serializer::spectrumType()const
{
	return UINT_VALUE_IN_THE_OFFSET(OFFSETS::_SPECTRUM_TYPE_);
}


uint32_ttt pitz::adc::data::Serializer::eventNumber()const
{
	return UINT_VALUE_IN_THE_OFFSET(OFFSETS::_EVENT_NUMBER_);
}


uint32_ttt pitz::adc::data::Serializer::seconds()const
{
	return UINT_VALUE_IN_THE_OFFSET(OFFSETS::_SECONDS_);
}


uint32_ttt pitz::adc::data::Serializer::microSeconds()const
{
	return UINT_VALUE_IN_THE_OFFSET(OFFSETS::_MCR_SECONDS_);
}


void pitz::adc::data::Serializer::setSerializerVersion(uint32_ttt a_unSerialierVersion)
{
	UINT_VALUE_IN_THE_OFFSET(OFFSETS::_SERIALIZER_VERSION_) = a_unSerialierVersion;
}


void pitz::adc::data::Serializer::setSpectrumType(uint32_ttt a_unSpectrumType)
{
	UINT_VALUE_IN_THE_OFFSET(OFFSETS::_SERIALIZER_VERSION_) = a_unSpectrumType;
}


void pitz::adc::data::Serializer::setEventNumber(uint32_ttt a_unEventNumber)
{
	UINT_VALUE_IN_THE_OFFSET(OFFSETS::_SERIALIZER_VERSION_) = a_unEventNumber;
}


void pitz::adc::data::Serializer::setSeconds(uint32_ttt a_unSeconds)
{
	UINT_VALUE_IN_THE_OFFSET(OFFSETS::_SERIALIZER_VERSION_) = a_unSeconds;
}


void pitz::adc::data::Serializer::setMicroSeconds(uint32_ttt a_unMicroSeconds)
{
	UINT_VALUE_IN_THE_OFFSET(OFFSETS::_SERIALIZER_VERSION_) = a_unMicroSeconds;
}
