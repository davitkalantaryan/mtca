/*****************************************************************************
 * File                 : main_sis8300_initialize.cpp
 *                         (initially: sis8300_initialize_main.cpp)
 * created              : 2016 March 24
 * last modification    : 2017 Jun 9
 *****************************************************************************
 * Author               :	D.Kalantaryan, Tel:033762/77552 kalantar
 * Email                :	davit.kalantaryan@desy.de
 * Mail                 :	DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 *   file for ...
 ****************************************************************************/

#define SIMPLE_INIT_DEVICE

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>

#include <mtsys/adc_timer_interface_io.h>
#include <sis8300.h>
#include <common_argument_parser.hpp>

static void PrintHelp(void);

int main(int a_argc, char* a_argv[])
{
    MTCA_CLASSES::sis8300 aSisdevice;
    const char* cpcDeviceName = "";
    const char* cpcDmaTrgName = NULL;
    const char* cpcGenEvntName = NULL;
    int nNumberOfSamples;
    int nClockType;
    int nTrigerType;
    int nTriggerSource;
    u_int8_t   vDivides[_NUMBER_OF_ADCS_];
    u_int32_t unResetSmpLogic = 0x00004;
    const char* cpcMode = "";
    int nDivider = 2;

    common::argument_parser aParser;

    aParser.AddOption("--device",1,"");
    aParser.AddOption("-d", 1,"");
    aParser.AddOption("--deviders",1,"2");
    aParser.AddOption("-dv", 1,"2");
    aParser.AddOption("--mode", 1,"");
    aParser<<"--help"<<"-h";

    if(a_argc>1)
    {
        char** argv = a_argv + 1;
        int nArgs(a_argc-1);
        aParser.ParseCommandLine<int&,char*>(nArgs, argv);

        if (aParser["--mode"]) { cpcMode = argv[0]; }
        else if(nArgs){ cpcMode = argv[0]; }
    }

    if(aParser["--help"] || aParser["-h"]){
        PrintHelp();
        printf("%s\n", aParser.HelpString().c_str());
        return 0;
    }
    else if( aParser["--device"]  ){cpcDeviceName=aParser["--device"];}
    else if( aParser["-d"]  )      {cpcDeviceName=aParser["-d"];}
    else
    {
        fprintf(stderr,"Provide device name!\n");
        PrintHelp();
        printf("%s\n", aParser.HelpString().c_str());
        return 1;
    }

    if(aParser["--deviders"]){nDivider=atoi(aParser["--deviders"]);}
    else if(aParser["-dv"]){nDivider=atoi(aParser["-dv"]);}


    if(strcmp(cpcMode,"help") == 0)
    {
        PrintHelp();
        return 0;
    }
    else if(strcmp(cpcMode,"internal") == 0)
    {
        nClockType = SIS8300_CLOCK_INTERNAL;
        nTrigerType = 0;  // 2
        nTriggerSource = TRG_SRC_BACKPLANE_RX18;
        cpcDmaTrgName = "";
        cpcGenEvntName = "";
    }
    else if(strcmp(cpcMode,"x2timer") == 0)
    {
        nClockType = SIS8300_BACKPLANE_CLK1;
        nTrigerType = 2;  // 2
        nTriggerSource = TRG_SRC_BACKPLANE_RX18;
        cpcDmaTrgName = X2TIMER_WAITERS;
        cpcGenEvntName = X2TIMER_GENEVNT;
    }
    else if( strcmp(cpcMode,"ip8timer") == 0 )
    {
        nClockType = SIS8300_FRONT_EXTCLCB_HARLINK;
        //nClockType = SIS8300_BACKPLANE_CLK1; ///??????????
        nTrigerType = 2;  // 2
        //nTriggerSource = TRG_SRC_BACKPLANE_RX18; ///??????????
        nTriggerSource = TRG_SRC_HARLINK_IN_OUT;
        cpcDmaTrgName = IPTIMER_WAITERS;
        cpcGenEvntName = IPTIMER_GENEVNT;
    }
    else
    {
        PrintHelp();
        return -2;
    }

    printf("dev_name=\"%s\"\n",cpcDeviceName);

    if(!aSisdevice.open(cpcDeviceName))
    {
        fprintf(stderr,"Could not open the file \"%s\"\n",cpcDeviceName);
        perror("\n");
        return -3;
    }

    nNumberOfSamples = aSisdevice.GetNumberOfSamples();
    if(nNumberOfSamples<=0)
    {
        fprintf(stderr,"Could not get number of samples\n");
        perror("\n");
        aSisdevice.close();
        return -3;
    }

    vDivides[0] = nDivider;
    vDivides[1] = nDivider;
    vDivides[2] = nDivider;
    vDivides[3] = nDivider;
    vDivides[4] = nDivider;

    // reset sample logic
    aSisdevice.write(0,SIS8300_ACQUISITION_CONTROL_STATUS_REG*4,1,&unResetSmpLogic,RW_D32 ); //

    //s_sisdevice.InitDevice(SIS8300_CLOCK_INTERNAL,100,nNumberOfSamples,2,TRG_SRC_BACKPLANE_RX18,vDivides);
    // edit by BP
    printf(
                "init device parameters:\n"
                "\tmode(%s)\n"
                "\tdevidersValue: {%d,%d},\n"
                "\tclock type %d\n"
                "\tpretrigger delay 100\n"
                "\tnumber of samples %d\n"
                "\ttrigger type %d\n"
                "\ttrigger source %d\n",
                cpcMode,
                nDivider,nDivider,
                nClockType, nNumberOfSamples, nTrigerType, nTriggerSource);
    // end edit by BP
    aSisdevice.InitDevice(nClockType,100,nNumberOfSamples,nTrigerType,nTriggerSource,vDivides);
    //s_sisdevice.InitDevice(SIS8300_BACKPLANE_CLK2,100,nNumberOfSamples,2,TRG_SRC_BACKPLANE_RX18,vDivides);

    //aSisdevice.SetDmaTriggerSource(const char* source_name);
    //int         SetGenEventSource(const char* source_name);
    aSisdevice.SetDmaTriggerSource(cpcDmaTrgName);
    aSisdevice.SetGenEventSource(cpcGenEvntName);

    aSisdevice.close();

    return 0;
}



static void PrintHelp(void)
{
    printf("All options :\n"
           "     --device (-d)                 : Selects the device\n"
           "     --deviders (-dv)              : Selects the devider value\n"
           "All modes   :\n"
           "     help\n"
           "     internal\n"
           "     x2timer\n"
           "     ip8timer\n");
}
