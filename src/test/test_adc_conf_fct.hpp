#ifndef ADC_CONF_FCT_H
#define ADC_CONF_FCT_H

#define FCT 463

#include <eq_fct.h>
#include "sis8300.h"

class ADC_conf_fct : public EqFct
{
public:
    ADC_conf_fct();

private:
    void init() override ;
    void cancel() override;
    int fct_code() override;
    void update() override;

    int initializeRaw();

private:
    D_string    m_deviceName;
    D_int       m_devider0;
    D_int       m_devider1;
    D_int       m_devider2;
    D_int       m_devider3;
    D_int       m_devider4;
    D_int       m_numOfSamples;
    D_int       m_pretrigerDelay;
    D_int       m_triggerSource;

    MTCA_CLASSES::sis8300         m_sis8300raw;

    bool        m_bOk;

};

#endif // ADC_CONF_FCT_H
