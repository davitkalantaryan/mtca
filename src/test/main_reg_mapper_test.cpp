/*****************************************************************************
 * File                 : main_reg_mapper_test.cpp
 *                         (initially: sis8300_initialize_main.cpp)
 * created              : 2017 Jul 26
 * last modification    : 2017 Jul 26
 *****************************************************************************
 * Author               :	D.Kalantaryan, Tel:033762/77552 kalantar
 * Email                :	davit.kalantaryan@desy.de
 * Mail                 :	DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 *   file for ...
 ****************************************************************************/

#define SIMPLE_INIT_DEVICE

#include <stdio.h>
#include <time.h>
#include <string.h>
#include <stdlib.h>
#include <sys/time.h>
#include <stdint.h>

#include <mtsys/adc_timer_interface_io.h>
#include <common_argument_parser.hpp>
#include <mtca_devregmapper.hpp>

enum RUN_TYPES{READ_ONE_BYTE,READ_ONE_SAMPLE};

static void PrintHelp(void);
static u_int8_t ReadOneByte(const u_int8_t* a_pBuffer, int a_nOffset);
/* Channel number [1-8] */
static u_int16_t ReadOneSample12bit(const u_int8_t* a_pBuffer, int a_nChannelNumber);
u_int16_t ReadOneSample16bit(const u_int8_t* a_pBuffer, int a_nChannelNumber);

int main(int a_argc, char* a_argv[])
{
    const char* cpcMode = "dummy";
    const char* cpcDeviceName = "";
    u_int8_t* pcBuffer;
    u_int16_t tValue;
    common::argument_parser aParser;
    mtca::DevRegMapper  aMapper;
    int nOffset(1),nChannel(1);
    int nMode(READ_ONE_BYTE);
    int nIpNumber(2);
    //float fKoeficient((float)0x0fff);
    float fKoeficient((float)1);

    aParser.AddOption("--device",1,"");
    aParser.AddOption("-d", 1,"");
    aParser.AddOption("--offset",1,"1");
    aParser.AddOption("-o", 1,"1");
    aParser.AddOption("--channel-number",1,"1");
    aParser.AddOption("-cn", 1,"1");
    aParser.AddOption("--ip-number",1,"1");
    aParser.AddOption("-in", 1,"1");
    aParser.AddOption("--mode", 1,"");
    aParser<<"--help"<<"-h";

    if(a_argc>1)
    {
        char** argv = a_argv + 1;
        int nArgs(a_argc-1);
        aParser.ParseCommandLine<int&,char*>(nArgs, argv);

    }

    if(aParser["--help"] || aParser["-h"]){
        PrintHelp();
        printf("%s\n", aParser.HelpString().c_str());
        return 0;
    }
    else if( aParser["--device"]  ){cpcDeviceName=aParser["--device"];}
    else if( aParser["-d"]  )      {cpcDeviceName=aParser["-d"];}
    else
    {
        fprintf(stderr,"Provide device name!\n");
        PrintHelp();
        printf("%s\n", aParser.HelpString().c_str());
        return 1;
    }

    if( aParser["--offset"]  ){nMode=READ_ONE_BYTE;nOffset=common::arg_parser::StringToInt(aParser["--offset"]);}
    else if( aParser["-o"]  ) {nMode=READ_ONE_BYTE;nOffset=common::arg_parser::StringToInt(aParser["-o"]);}

    if( aParser["--channel-number"]){nMode=READ_ONE_SAMPLE;nChannel=common::arg_parser::StringToInt(aParser["--channel-number"]);}
    else if( aParser["-cn"]  ) {nMode=READ_ONE_SAMPLE;nChannel=common::arg_parser::StringToInt(aParser["-cn"]);}

    if( aParser["--ip-number"]  ){nIpNumber=common::arg_parser::StringToInt(aParser["--ip-number"]);}
    else if( aParser["-in"]  ) {nIpNumber=common::arg_parser::StringToInt(aParser["-in"]);}



    if(strcmp(cpcMode,"help") == 0)
    {
        PrintHelp();
        return 0;
    }
#if 0
    else
    {
        PrintHelp();
        return -2;
    }
#endif

    pcBuffer = (u_int8_t*)aMapper.openAndMapC(cpcDeviceName,3);

    if(pcBuffer==MAP_FAILED){
        goto returnPoint;
    }

    switch(nMode)
    {
    case READ_ONE_BYTE:
        tValue = ReadOneByte(pcBuffer,nOffset);
        printf("dev_name=\"%s\";offset=0x%X;code=0x%X;ascii=%c\n",cpcDeviceName,nOffset,tValue,(char)tValue);
        break;
    default:
        nOffset = 0x100*nIpNumber;
        tValue = ReadOneSample16bit(pcBuffer+nOffset,nChannel);
        //printf("dev_name=\"%s\";channel=%d;value=%f\n",cpcDeviceName,nChannel,(float)tValue/fKoeficient);
        printf("dev_name=\"%s\";channel=%d;value=%d\n",cpcDeviceName,nChannel,(int)tValue);
        break;
    }


    goto returnPoint;

returnPoint:
    aMapper.closeC();

    return 0;
}



static void PrintHelp(void)
{
    printf("All options :\n"
           "     --device (-d)                 : Selects the device\n"
           "     --deviders (-dv)              : Selects the devider value\n"
           "All modes   :\n"
           "     help\n"
           "     internal\n"
           "     x2timer\n"
           "     ip8timer\n");
}


static u_int8_t ReadOneByte(const u_int8_t* a_devRegs, int a_nOffset)
{
    int nOffsetToRead( (a_nOffset>>1)<<1 );
    u_int16_t tValue( *((u_int16_t*)(a_devRegs + nOffsetToRead)) );

    if(a_nOffset&1){tValue&=0x00ff;}else{tValue>>=8;}
    return (u_int8_t)tValue;
}


static u_int8_t WriteOneByte(u_int8_t* a_devRegs, int a_nOffset, u_int8_t a_value)
{
    int nOffsetToRead( (a_nOffset>>1)<<1 );
    u_int16_t tValue( *((u_int16_t*)(a_devRegs + nOffsetToRead)) );

    if(a_nOffset&1){tValue|=a_value;}else{tValue>>=8;}
    return (u_int8_t)tValue;
}


/* Channel number [1-8] */
static u_int16_t ReadOneSample12bit(const u_int8_t* a_devRegs, int a_nChannelNumber)
{
    int nOffset(4*(a_nChannelNumber-1)+1);
    u_int16_t tDataHigh(((u_int16_t)ReadOneByte(a_devRegs,nOffset))<<4);
    u_int16_t tDataLow(((u_int16_t)ReadOneByte(a_devRegs,nOffset+2))>>4);

    return tDataHigh|tDataLow;
}


/* Channel number [1-8] */
u_int16_t ReadOneSample16bit(const u_int8_t* a_pBuffer, int a_nChannelNumber)
{
    int nOffset(2*(a_nChannelNumber-1)+1);
    u_int16_t tDataHigh(((u_int16_t)ReadOneByte(a_pBuffer,nOffset))<<8);
    u_int16_t tDataLow(ReadOneByte(a_pBuffer,nOffset+2));

    return tDataHigh|tDataLow;
}


static void MakeSamples(u_int8_t* a_devRegs,u_int16_t* a_pBuffers[],int a_nWaitMs)
{
    //
}
