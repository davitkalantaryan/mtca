#include "test_adc_conf_fct.hpp"
#include "mtsys/adc_timer_interface_io.h"

#if 0
D_string    m_deviceName;
D_int       m_devider0;
D_int       m_devider1;
D_int       m_devider2;
D_int       m_devider3;
D_int       m_devider4;
D_int       m_numOfSamples;
#endif

ADC_conf_fct::ADC_conf_fct():
    EqFct("Name = location"),
    m_deviceName("DEVICE_NAME", this),
    m_devider0("DIVIDER.0 comment for clients",this),
    m_devider1("DIVIDER.1 comment for clients",this),
    m_devider2("DIVIDER.2 comment for clients",this),
    m_devider3("DIVIDER.3 comment for clients",this),
    m_devider4("DIVIDER.4 comment for clients",this),
    m_numOfSamples("NUMBER_OF_SAMPLES", this),
    m_pretrigerDelay("PRETRIGGER.DELAY",this),
    m_triggerSource("TRIGGER.SOURCE",this)
{
    m_bOk = false;
    m_deviceName.set_ro_access();
    m_numOfSamples.set_ro_access();
}

void ADC_conf_fct::init()
{
    initializeRaw();
}

void ADC_conf_fct::cancel()
{
    m_sis8300raw.close();
}

int ADC_conf_fct::fct_code()
{
    return FCT;
}


void ADC_conf_fct::update()
{
    if(!m_bOk){initializeRaw();}
}


int ADC_conf_fct::initializeRaw()
{
    const char* cpcDmaTrgName = X2TIMER_WAITERS;
    const char* cpcGenEvntName = X2TIMER_GENEVNT;
    const char* cpcDeviceName = m_deviceName.value();
    int nClockType = SIS8300_CLOCK_INTERNAL;
    int nNumberOfSamples, nPretriggerDelay;
    int nTriggerSource ;
    int nTrigerType = 2;
    u_int32_t unResetSmpLogic = 0x00004;
    u_int8_t   vDivides[_NUMBER_OF_ADCS_];

    // Following 4 lines should be modified
    m_pretrigerDelay.set_value(100);
    m_triggerSource.set_value(TRG_SRC_BACKPLANE_RX18);
    m_pretrigerDelay.set_ro_access();
    m_triggerSource.set_ro_access();

    if(!m_sis8300raw.open(cpcDeviceName))
    {
        fprintf(stderr,"Could not open the file \"%s\"\n",cpcDeviceName);
        perror("\n");
        // Set device offline
        return -3;
    }

    nNumberOfSamples = m_sis8300raw.GetNumberOfSamples();
    m_numOfSamples.set_value(nNumberOfSamples);

    if(nNumberOfSamples<=0)
    {
        fprintf(stderr,"Could not get number of samples\n");
        perror("\n");
        m_sis8300raw.close();
        // set device offline
        return -3;
    }

    nTriggerSource = m_triggerSource.value(); // new

    vDivides[0]=m_devider0.value();
    vDivides[1]=m_devider1.value();
    vDivides[2]=m_devider2.value();
    vDivides[3]=m_devider3.value();
    vDivides[4]=m_devider4.value();

    // Bacatrel inchu e set@ arvum
    m_sis8300raw.write(0,SIS8300_ACQUISITION_CONTROL_STATUS_REG*4,1,&unResetSmpLogic,RW_D32 );

    printf(
                "init device parameters:\n"
                "deviceName=\"%s\"\n"
                "\tmode(x2timer)\n"
                "\tdevidersValue: {%d,%d,%d,%d,%d},\n"
                "\tclock type %d\n"
                "\tpretrigger delay 100\n"
                "\tnumber of samples %d\n"
                "\ttrigger type %d\n"
                "\ttrigger source %d\n",
                cpcDeviceName,
                vDivides[0],vDivides[1],vDivides[2],vDivides[3],vDivides[4],
                nClockType, nNumberOfSamples, nTrigerType, nTriggerSource);

    nPretriggerDelay = m_pretrigerDelay.value(); // new

    m_sis8300raw.InitDevice(nClockType,nPretriggerDelay,nNumberOfSamples,nTrigerType,nTriggerSource,vDivides);

    m_sis8300raw.SetDmaTriggerSource(cpcDmaTrgName);
    m_sis8300raw.SetGenEventSource(cpcGenEvntName);

    m_bOk = true;

    return 0;
}
