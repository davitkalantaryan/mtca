/*****************************************************************************
 * File			: main_serializer_test.cpp
 * Created on	: 2017 Jun 15
 *****************************************************************************
 * Created by	: D.Kalantaryan, Tel:+49(0)33762/77552 kalantar
 * Email		: davit.kalantaryan@desy.de
 * Mail			: DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 *   This is the file for the main funtion for testing
 *   the class pitz::adc::data::Serializer
 ****************************************************************************/
#include <stdio.h>
//#include "pitz_adc_data_serializer.hpp"
#include "adc_data_serializer.h"


void* (*g_fpPonter)(void*);


int main()
{
	//pitz::adc::data::Serializer aSeri;
	//adc_data_serializer aSeri;

	//aSeri.set_spectrum_size(1024);

	g_fpPonter(NULL);
	//(*g_fpPonter)(NULL);


	return 0;
}

