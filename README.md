# mtca

## Some hints  

### setup MTCA from web  
```  
wget https://desycloud.desy.de/index.php/s/a5A9sNtG5dJsafC/download?path=%2Fubuntu18%2Fscripts\&files=prepare_zn_mtca_host.sh -O prepare_zn_mtca_host.sh && chmod a+x prepare_zn_mtca_host.sh && ./prepare_zn_mtca_host.sh    
```  

### todo:  
 - [change loging to this](https://help.ubuntu.com/community/SingleSignOn)  

In order to manipulate auth mechanizm, make following   
``` bash
pam-auth-update
```  

## Some useful likns  

[web](https://davitkalantaryan.github.io/mtca/)  
[github](https://github.com/davitkalantaryan/mtca)  
[bitbucket](https://bitbucket.org/davitkalantaryan/mtca/src/master/)  
[DESY stash](https://stash.desy.de/users/kalantar/repos/mtca/browse)    
