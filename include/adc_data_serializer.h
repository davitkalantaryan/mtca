#pragma once

/*****************************************************************************
 * Description
 *   Header info
 *   Fields
 *		1) 2      B [ 0  -1         ]   Start maker '$', 'S'
 *		1) 4      B [ 2  -4         ]   Serializer version (for backward compatibility)
 *		2) 4      B [ 6  -7         ]   Spectrum Type (float = 0, double = 1)
 *		3) 4      B [ 10  -13       ]   Spectrum size 
 *		4) 4      B [ 14 -17        ]   Event Number ()
 *		5) 4	  B [ 18 -21		]	Seconds (keeps the seconds of the sampling start time)
 *		6) 4	  B [ 22 -25		]	Microseconds (keeps the microseconds of the sampling start time)
 *		7) 8	  B [ 26 -31		]	Reserved
 *		8) X*Y	  B [ 34 -32+(X*Y)  ]   Spectrum (X-number of elements, Y-element size)
 *
 ****************************************************************************/


#include <cstdint>
#include <string>

#if !defined(BIG_ENDIAN_BP) & !defined(LITTLE_ENDIAN_BP)
#error Either BIG_ENDIAN_BP or LITTLE_ENDIAN_BP must be defined
#endif

#if defined(BIG_ENDIAN_BP) & defined(LITTLE_ENDIAN_BP)
#error Both BIG_ENDIAN_BP and LITTLE_ENDIAN_BP are defined
#endif

namespace ADC_SERIALIZER{ enum { ARAY_DATA_FLOAT, ARAY_DATA_DOUBLE };}
typedef void*(*typeSwapFunction)(void*);

class adc_data_serializer
{

public:
	
	/////////////////////////////////////////////////////////////////////////////////
	// constructor ( creates float type )
	adc_data_serializer();
	
	// destructor
	virtual ~adc_data_serializer();
	
	//////////////////////////////////////////////////////////////////////////////////
	// header set API
	// set serializer version
	//void set_version(int32_t aVersion); // version should be set according to 

	// set spectrum type: float -> 0, double -> 1
	void set_spectrum_type(int32_t aSpectrumType);

		// set spectrum size
	void set_spectrum_size(int32_t aSpectrumSize);

	// set event number to internal buffer
	void set_event_number(int32_t aEventNumber);

	// set event time seconds
	void set_time_seconds(int32_t aSeconds);

	void set_time_useconds(int32_t aUSeconds);

	//////////////////////////////////////////////////////////////////////////////////
	// header get API
	// get serializer version
	int32_t get_version()const;
	
	// get spectrum type
	int32_t get_spectrum_type()const;
	
	// get spectrum size
	int32_t get_header_size()const;
	
	// get spectrum size
	int32_t get_spectrum_size()const;
	
	// get buffer size
	int32_t get_buffer_size()const;
	
	// get event number
	int32_t get_event_number()const;
	
	// get time seconds
	int32_t get_time_seconds()const;
	
	// get time microseconds
	int32_t get_time_useconds()const;
	
	//////////////////////////////////////////////////////////////////////////////////
	// spectrum set API
	// set spectrum item
	// problem: if array type is set to double, but template function is constructed with float
	// then data is corrupted
	template <typename T> void set_spectum_item(int aIndex, T aValue);
	
	// set spectrum array
	template <typename T> void set_spectum_array(T* aTArray);
	
	//////////////////////////////////////////////////////////////////////////////////
	// spectrum get API
	// makes spectrum area swapping in necessary
	template <typename T> void prepare_spectrum();
	
	// get spectrum item
	// warning: must be called after void adc_data_serializer::prepare_spectrum()
	template <typename T> T get_spectrum_item(int aIndex);
	
	// get spectrum array
	// warning: must be called after void adc_data_serializer::prepare_spectrum()
	template <typename T> const T* get_spectrum_array()const;
	template <typename T> T* get_spectrum_array();
	

	// returns data buffer pointer
	// warning: must be called after void adc_data_serializer::prepare_spectrum()
	const uint8_t* get_buffer()const;
	uint8_t* get_buffer();
	
	// returns content of data buffer as std string
	// warning: must be called after void adc_data_serializer::prepare_spectrum()
	std::string toString();

protected:
	//static const int header_size = 64;
	uint8_t* data_buffer;
	typeSwapFunction m_swap_bytes;

	static void* swap_4_byte(void* a_src);
	static void* swap_8_byte(void* a_src);

private:
	static const int header_size = 64;

};

#include "adc_data_serializer.impl.h"
