/*****************************************************************************
 * File		  mtca_devregmapper.hpp
 * created on 2017-07-26
 *****************************************************************************
 * Author:	D.Kalantaryan, Tel:033762/77552 kalantar
 * Email:	davit.kalantaryan@desy.de
 * Mail:	DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 *   file for ...
 ****************************************************************************/
#ifndef __MTCA_DEVREGMAPPER_HPP__
#define __MTCA_DEVREGMAPPER_HPP__


#ifdef WIN32
#include <io.h>
#include <linux/ioctl.h>
#else
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#endif

#include <signal.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include "mtsys/pciedev_io.h"

namespace mtca
{
class DevRegMapper
{
public:
    DevRegMapper();
    virtual ~DevRegMapper();

    virtual void* openAndMapC(const char* devName,int bar, int flag=O_RDWR);
    virtual void closeC();

protected:
    int     m_fd;
    void*   m_pBase;
};
}

#endif // #ifndef __MTCA_DEVREGMAPPER_HPP__
