/*
 *	File: mtcagen_local.h
 *
 *	Created on: Apr 20, 2016
 *	Authors:
 *      Davit Kalantaryan (Email: davit.kalantaryan@desy.de)
 *
 *
 */
#ifndef MTCAGEN_LOCAL_H
#define MTCAGEN_LOCAL_H

#define OFFSET_TO_REGISTER_DATA     0
#define INTERESTED_AREA_MAX_SIZE    ((int)((__SI_PAD_SIZE - OFFSET_TO_REGISTER_DATA) * 4 ))

#include "mtca_upciedevlocal.hpp"
#include <pthread.h>
#include <vector>
#include "lightweight_mutex.h"

#define ARGUMENT_LIST   int a_bar,int a_first_reg_adr,int a_num_of_regs,const unsigned char*a_data
typedef void (*TYPE_UPDATE)(void*clb_data,ARGUMENT_LIST);

typedef struct SSingleEntry
{
    //TYPE_UPDATE callback;
    void (*m_callback)(void*clb_data,ARGUMENT_LIST);
    void*       callback_data;
    int         key;
    int         bar;
    int         first_reg_address;
    int         number_of_interested_regs;
    int         size_of_interested_regs_in_bytes;
}SSingleEntry;

namespace MTCA_CLASSES
{

class mtcagen_local : public mtca::UpciedevLocal
{
public:
    mtcagen_local(int fd=-1);
    mtcagen_local(const char* devName,int flag=O_RDWR);
    virtual ~mtcagen_local();

    int register_for_changes_st(void* callback_data,TYPE_UPDATE update_func,
                                int bar, int first_reg_address,int num_of_regs);

    template <typename cls_dt_type>
    int register_for_changes(cls_dt_type* a_cls_ins,void (cls_dt_type::*a_cls_fnc)(ARGUMENT_LIST),
                             int a_bar, int a_first_reg_address,int a_num_of_regs)
    {
        TYPE_UPDATE fpFunc = (TYPE_UPDATE)GetFunctionPointer(1,a_cls_fnc);
        return register_for_changes_st(a_cls_ins,fpFunc,
                                       a_bar,a_first_reg_address,a_num_of_regs);
    }

    void delete_entry(int key);

    //int open(const char* devName,int flag=O_RDWR);
    //void close();

protected:
    static void* ThreadFuncUpdate(void* a_this);
    void* ThreadFuncUpdate();
    static void* GetFunctionPointer(int first, ...);

private:
    pthread_t                           m_UpdateThread;
    volatile int                        m_bUpdate;
    std::vector<SSingleEntry>           m_Entries;
    ADAV_CLASSES::lightweight_mutex     m_mutex;

    /// Will be deleted
    mutable std::vector<unsigned char*> m_buffers;
};

}

#endif // MTCAGEN_LOCAL_H
