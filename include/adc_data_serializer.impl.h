#pragma once
#include <cstring>
#include <memory.h>

#define VALUE_FOR_THE_INDEX(_a_type,_a_index)	((_a_type*)(data_buffer + header_size))[(_a_index)]

#ifdef BIG_ENDIAN_BP
#define SWAP_AND_SET_ITEM(_a_type,_a_index,_a_value)	\
												(*m_swap_bytes)(&(_a_value));\
												VALUE_FOR_THE_INDEX(_a_type,_a_index)=(_a_value)
#define GET_AND_SWAP_ITEM(_a_index,_a_value)	(_a_value) = UINT_VALUE_IN_THE_OFFSET2(_a_offset);\
												swap_4_byte(&(_a_value));
#else   // #ifdef BIG_ENDIAN_BP
#define SWAP_AND_SET_ITEM(_a_type,_a_index,_a_value)	VALUE_FOR_THE_INDEX(_a_type,_a_index)=(_a_value)
#define GET_AND_SWAP_INT(_a_offset,_a_value)	(_a_value) = UINT_VALUE_IN_THE_OFFSET2(_a_offset);
#endif  // #ifdef BIG_ENDIAN_BP

namespace ADC_SERIALIZER
{
	enum {
		OFFSET_VERSION  = 4,
		OFFSET_TYPE     = 8,
		OFFSET_SIZE     = 12,
		OFFSET_EVENT    = 16,
		OFFSET_SECONDS  = 20,
		OFFSET_USECONDS = 24
	};
}
		
//////////////////////////////////////////////////////////////////////////////////
// spectrum set API
// set spectrum item
template <typename T>
	void adc_data_serializer::set_spectum_item(int aIndex, T aValue)
	{
		SWAP_AND_SET_ITEM(T, aIndex, aValue);
	}
	
// set spectrum array
template <typename T>
	void adc_data_serializer::set_spectum_array(T* aTArray)
	{
#ifdef BIG_ENDIAN_BP		//for (int i(0); i < get_spectrum_size(); i += sizeof(T))		for (int i(0); i < get_spectrum_size(); ++i)		{			set_spectum_item(i, aTArray[i]);		}
#else
		memcpy(data_buffer + header_size, aTArray, get_spectrum_size());
#endif
	}
	
//////////////////////////////////////////////////////////////////////////////////
// spectrum get API
// makes spectrum area swap if necessary
template <typename T>
	void adc_data_serializer::prepare_spectrum()
	{
#ifdef BIG_ENDIAN_BP
		for (int i(0); i < get_spectrum_size(); i += sizeof(T))		{			(*m_swap_bytes)(data_buffer + adc_data_serializer::header_size + i*sizeof(T));		}
#endif
	}

	// get spectrum item
	// warning: must be called after void adc_data_serializer::prepare_spectrum()
template <typename T>
	T adc_data_serializer::get_spectrum_item(int aIndex)
	{
		//return *((T*)(data_buffer + adc_data_serializer::header_size + aIndex*sizeof(T)));
		return VALUE_FOR_THE_INDEX(T, aIndex);
	}
	
// get spectrum array
// warning: must be called after void adc_data_serializer::prepare_spectrum()
template <typename T>
	const T* adc_data_serializer::get_spectrum_array()const
	{
		return (const T*)(data_buffer + header_size);		
	}
template <typename T>
	T* adc_data_serializer::get_spectrum_array()
	{
		return (T*)(data_buffer + header_size);		
	}
