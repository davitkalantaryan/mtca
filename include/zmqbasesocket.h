#pragma once

class BaseZmqSocket
{
protected:
	static void*    smv_zmq_context;
	static int      smn_instances;
	void*           m_zmq_socket;
	
public:
	BaseZmqSocket();  // throws const char*
	~BaseZmqSocket();
};

