/*****************************************************************************
 * File		  sis8300.h
 * created on 2015-07-31
 *****************************************************************************
 * Author:	D.Kalantaryan, Tel:033762/77552 kalantar
 * Email:	davit.kalantaryan@desy.de
 * Mail:	DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 *   file for ...
 ****************************************************************************/
#ifndef SIS8300_H
#define SIS8300_H

#define _PITI_JNJVI_

#define _NUMBER_OF_ADCS_            5
#define _DIVIDER_MAX_VALUE_         32

#define SIS8300_IDENTIFIER_VERSION_REG  		0x00
#define SIS8300_FIRMWARE_OPTIONS_REG  			0x05
#define SIS8300_ACQUISITION_CONTROL_STATUS_REG		0x10
#define SIS8300_L_MLVDS_IO_CONTROL_REG          0x12
#define SIS8300_HARLINK_IN_OUT_CONTROL_REG		0x13
#define SIS8300_ADC_SPI_REG                		0x48
#define SIS8300_AD9510_SPI_REG             		0x41
#define SIS8300_CLOCK_MULTIPLIER_SPI_REG        0x42
#define SIS8300_TRIGGER_SETUP_CH1_REG			0x100
#define SIS8300_TRIGGER_THRESHOLD_CH1_REG		0x110
#define SIS8300_PRETRIGGER_DELAY_REG			0x12B
#define DDR2_ACCESS_CONTROL 0x230

#define AD9510_GENERATE_SPI_RW_CMD			0x40000000
#define AD9510_SPI_SELECT_NO2				0x01000000
#define AD9510_SPI_SET_FUNCTION_SYNCH_FPGA_CLK69	0x10000000
#define AD9510_GENERATE_FUNCTION_PULSE_CMD		0x80000000

#define SIS8300_ERROR   -1
#define SIS8300_CLOCK_INTERNAL  0x0000000f
#define SIS8300_BACKPLANE_CLK1  0x0000000a
#define SIS8300_BACKPLANE_CLK2  0x00000005
#define SIS8300_FRONT_EXTCLCA_SMA   0x00000150
#define SIS8300_FRONT_EXTCLCB_HARLINK   0x00000140

#define TRG_SRC_HARLINK_IN_OUT  1
#define TRG_SRC_BACKPLANE_RX18      2


#include "mtcagen_local.h"
#include <mtsys/sis8300_io.h>
#include <sys/mman.h>


namespace MTCA_CLASSES
{

class sis8300 : public mtcagen_local
{
public:
    void*       mmap(void *addr, size_t length, int prot, int flags,off_t offset);
    int         SetClockType(int type);
    int         GetClockType()const;
    const char* GetClockTypeString()const;
    int         SetNumberOfSamples(int number);
    u_int32_t   GetNumberOfSamples()const;
    bool        IsChannelEnable(int channel)const;
    void        SetChannelEnable(int channel,bool enable);
    int         SetOffsets();
    int         ConfigureADCs();
    int         AD9510_SPI_Setup(u_int8_t* dividerValues, unsigned int ad9510_synch_cmd );
    int         InitDevice( int clockType,int pre_trigger_delay, int numSamples,
                            int trigger_type,int trigger_source, u_int8_t* divideValues);
    int         InitDevicePre2( int clockType,int pre_trigger_delay,
                                int trigger_type, int trigger_source, u_int8_t* divideValues);
    int         SetDivders(u_int8_t* divideValues);
    int         ADC_SPI_SetupDepr();
    int         SetDmaTriggerSource(const char* source_name);
    int         SetGenEventSource(const char* source_name);
    int         SI5326_set_clock_multiplier(int a_coeficient);
    int         SI5326_set_clock_multiplier_250MHz(int a_coeficient);
    int         si5326_clk_muliplier_write(unsigned int addr, unsigned int data);
    int         si5326_clk_muliplier_read(unsigned int addr, unsigned int *data_ptr);
    int         si5326_clk_muliplier_internal_calibration_cmd();
    int         si5326_get_status_external_clock_multiplier(unsigned int *status_ptr  );
    int         si5326_bypass_external_clock_multiplier();
    int         si5326_bypass_internal_ref_osc_multiplier();

private:
    int si5326_set_external_clock_multiplier_prvt( unsigned int bw_sel,
                                              unsigned int n1_hs,
                                              unsigned int n1_clk1, unsigned int n1_clk2,
                                              unsigned int n2_hs, unsigned int n2_ls,
                                              unsigned int a_n31);
    int ADC_SPI_SetupPrvt(unsigned int adc_device_no);

};

}

#endif // SIS8300_H
