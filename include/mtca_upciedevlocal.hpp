/*****************************************************************************
 * File		  mtca_upciedevlocal.hpp
 * created on 2017-05-20
 *****************************************************************************
 * Author:	D.Kalantaryan, Tel:033762/77552 kalantar
 * Email:	davit.kalantaryan@desy.de
 * Mail:	DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 *   file for ...
 ****************************************************************************/
#ifndef MTCA_UPCIEDEVLOCAL_HPP
#define MTCA_UPCIEDEVLOCAL_HPP


#ifdef WIN32
#include <io.h>
#include <linux/ioctl.h>
//#include <sys/utime.h>
#else
#include <unistd.h>
#include <sys/ioctl.h>
#endif

#include <signal.h>
#include "mtca_regbase.hpp"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <errno.h>
#include "mtsys/pciedev_io.h"

namespace mtca
{
class UpciedevLocal : public RegBase
{
public:
    UpciedevLocal(int a_fd=-1);

    UpciedevLocal(const char* devName,int flag=O_RDWR);

    virtual ~UpciedevLocal();

    virtual int open(const char* devName,int flag=O_RDWR);

    virtual void close();

    int ioctl(unsigned long request,void*arg=NULL);

    int DeviceIoControl(unsigned long request, void* in, int inLen, void* out, int* outLen);

    int read(int bar, int offset, int numberOfRegs, void* buffer, int reg_size=0xff)const;

    int write(int bar, int offset, int numberOfRegs, const void* buffer, int reg_size=0xff);

    int pread(int bar, int offset, int numberOfRegs, void* buffer,int reg_size=0xff)const;

    int pwrite(int bar, int offset, int numberOfRegs, const void* buffer,int reg_size=0xff);

    int set_bits(int bar, int offset, int numberOfRegs, const void* buf, const void* mask,int reg_size=0xff);

    int swap_bits(int bar, int offset, int numberOfRegs, const void* mask,int reg_size=0xff);

    int single_ioc_access(int bar, int offset, int numberOfRegs, const void* data,const void* mask,int access_mode,int reg_size=0xff);

    int vector_rw(int rwNumber, device_ioc_rw* iocRW);

    int lock_device();

    int unlock_device();

    operator const int&()const;

    UpciedevLocal& operator=(const int& fd);

    int GetSwap()const;

    void SetSwap(int swap);

    int  GetRegisterSizeMode()const;

    virtual int RegisterForInterrupts();

    virtual void UnRegisterFromInterrupts();

    virtual const char* GetInterupt(int timeout);

    ///////////////////////////////
    int GetRegAccesSizeInBytes(int register_size_mode)const;

    int GetDevNo()const;

protected:
    int     m_fd;
    union
    {
        char*   m_pcShared;
        siginfo_t   m_SigInfo;
    };
    int     m_nIRQtype;
};
}

#endif // #ifndef MTCA_UPCIEDEVLOCAL_HPP
