/*
 *	File: lightweight_mutex.h
 *
 *	Created on: Apr 20, 2016
 *	Authors:
 *      Davit Kalantaryan (Email: davit.kalantaryan@desy.de)
 *
 *
 */
#ifndef LIGHTWEIGHT_MUTEX_H
#define LIGHTWEIGHT_MUTEX_H

#ifdef WIN32
#include <WINDOWS.H>
#include <ERRNO.H>
typedef HANDLE  SYS_MUTEX;
#else   // #ifdef WIN32
#include <pthread.h>
typedef pthread_mutex_t  SYS_MUTEX;
#endif  // #ifdef WIN32


namespace ADAV_CLASSES
{

class lightweight_mutex
{
public:
    lightweight_mutex()
    {
#ifdef WIN32
        m_MutexLock = CreateMutex( NULL, FALSE, NULL );
#else

        pthread_mutexattr_t attr;
        pthread_mutexattr_init( &attr );
        pthread_mutexattr_settype( &attr, PTHREAD_MUTEX_ERRORCHECK );
        pthread_mutex_init( &m_MutexLock, &attr );
        pthread_mutexattr_destroy( &attr );
#endif
    }

    ~lightweight_mutex()
    {
#ifdef WIN32
        CloseHandle( m_MutexLock );
#else
        pthread_mutex_destroy( &m_MutexLock );
#endif
    }

    // int	timed_lock(int timeout_ms);

    int	lock()
    {
#ifdef WIN32
        return WaitForSingleObject( m_MutexLock, INFINITE ) == WAIT_OBJECT_0 ? 0 : EDEADLK ;
#else
        return pthread_mutex_lock( &m_MutexLock );
#endif
    }

    int	unlock()
    {
#ifdef WIN32
        return ReleaseMutex( m_MutexLock ) ? 0 : EPERM;
#else
        return pthread_mutex_unlock( &m_MutexLock );
#endif
    }

private:
    SYS_MUTEX	m_MutexLock;
};


}

#endif // LIGHTWEIGHT_MUTEX_H
