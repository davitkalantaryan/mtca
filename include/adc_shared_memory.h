/*
 *	File: adc_shared_memory.h
 *
 *	Created on: Mar 10, 2015
 *	Authors:
 *      Bagrat Petrosyan  (Email: bagrat.petrosyan@desy.de)
 *      Davit Kalantaryan (Email: davit.kalantaryan@desy.de)
 *
 *
 */
#ifndef ADC_SHARED_MEMORY_H
#define ADC_SHARED_MEMORY_H

#include <sys/types.h>
#include <mtcagen_io.h>

#define CURRENT_BUF_INDEX_FROM_CUR_BUF_PTR(__mem__) \
    (*((const int*)((const void*)((const char*)(__mem__) + __OFSET_TO_BUFFER_INDEX__))))

#define CURRENT_BUF_INDEX(__mem__)  (*((int*)(__mem__)))

#ifndef ADC_NUMBER_OF_CHANNELS
//#define ADC_NUMBER_OF_CHANNELS(__a_fd__) (ioctl((__fd__),ADC_NUMBER_OF_SAMPLES))
#define ADC_NUMBER_OF_CHANNELS(__a_fd__) 10
#endif
#ifndef _NUMBER_OF_CHANNELS_
#define _NUMBER_OF_CHANNELS_    ADC_NUMBER_OF_CHANNELS(this->m_fd)
#endif

#ifndef CHANNEL_PTR_IN_BUFFER
#define CHANNEL_PTR_IN_BUFFER(_buf_mem_,_channel_,_chan_size_)  ( ((char*)(_buf_mem_)) + __HEADER_SIZE__+ (_channel_)*(_chan_size_) )
#endif

#ifndef _DEPRICATED_FUNC_
#ifdef _MSC_VER
#define _DEPRICATED_FUNC_(_text_)	__declspec(deprecated(_text_))
#define _DEPRICATED_	_DEPRICATED_TEXT_("")
#else  // #ifdef _MSC_VER
#define	_DEPRICATED_FUNC_	__attribute__((deprecated))
#endif  // #ifdef _MSC_VER
#endif // #ifndef _DEPRICATED_FUNC_

/*
 * Rules of using this class is following
 * 1) Whenever device name is known 'connect_to_shared_memory' API should be called
 * 2) Then following should be done periodically
 *        a) 'wait_for_data'
 *        b) 'current_buffer_pointer'  (for handling buffer header)
 *        c) 'CURRENT_BUF_INDEX_FROM_CUR_BUF_PTR'
 *        d) 'event_number_in_specific_buffer'
 *        e) 'specific_buffer_specific_channel_pointer_new'
 *
 */

class adc_shared_memory
{
public:
    adc_shared_memory();
    adc_shared_memory(const char* dev_name);
    virtual ~adc_shared_memory();

    int connect_to_shared_memory(const char* dev_name);
    void disconnect_from_shared_memory();
    bool is_connected()const {return m_fd>0;}

    int number_of_samples()const;
    int slot_number()const;
    int wait_for_data(int timeoutMS);
    int wait_for_data_inf();

    int current_buffer_number()const _DEPRICATED_FUNC_ {return CURRENT_BUF_INDEX(m_mmaped_memory);}
    static int event_number_in_specific_buffer(const void* spec_buff);

    const void* current_buffer_pointer()const;
    const void* specific_buffer_pointer(int buffer_index)const;
    const void* current_buffer_specific_channel_pointer(int channel_number)const _DEPRICATED_FUNC_;
    const void* specific_buffer_specific_channel_pointer(int buffer_index,int channel_number)const _DEPRICATED_FUNC_;
    //const void* current_buffer_specific_channel_pointer(int channel_number)const _DEPRICATED_FUNC_;
    const void* specific_buffer_specific_channel_pointer_new(const void* spec_buff,int channel_number)const;

protected:
    void*   m_mmaped_memory;
    int     m_fd;
    int     m_nNumberOfSamples;
    int     m_nOneChannelSize;
    int     m_nOneBufferSize;
    //int     m_nSlotNumber;
    //int     m_nWholeMemSize; // It is used twice during creation and decreation
};

#endif // ADC_SHARED_MEMORY_H
