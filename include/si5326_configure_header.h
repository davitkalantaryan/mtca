/*****************************************************************************
 * File		  si5326_configure_header.h
 * created on 2016-03-24
 *****************************************************************************
 * Author:	D.Kalantaryan, Tel:033762/77552 kalantar
 * Email:	davit.kalantaryan@desy.de
 * Mail:	DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 *   file for ...
 ****************************************************************************/
#ifndef SI5326_CONFIGURE_HEADER_H
#define SI5326_CONFIGURE_HEADER_H

#ifndef SI5326_CONFIGURE_HEADER_NEEDED
#error not include header si5326_configure_header.h directly
#endif

typedef struct SClockConfigParams
{
    unsigned int bw_sel;
    unsigned int n1_hs;
    unsigned int n1_clk1;
    unsigned int n1_clk2;
    unsigned int n2_hs;
    unsigned int n2_ls;
    unsigned int n31;
}SClockConfigParams;

// 9.027775 MHz

static const SClockConfigParams s_cvConfigParams9MHz[] =
{
    {0,0,0,0,0,0,0},        // 0 not use this
    {10,4,144,144,5,576,5}, // 1 finally for 1 bypass should be configured
    {10,6,48,48,5,576,5},   // 2
    {10,5,36,36,6,450,5},   // 3
    {10,5,30,30,4,750,5},   // 4
    {10,11,10,10,11,250,5}, // 5
    {10,10,10,10,4,750,5},  // 6
    {10,6,14,14,5,588,5},   // 7
    {10,4,18,18,5,576,5},   // 8
    {10,11,6,6,11,270,5},   // 9
    {10,5,12,12,6,500,5},   // 10
    {10,7,8,8,10,308,5},    // 11
    {10,4,12,12,10,288,5}  // 12
};

static const int s_cnMaxMultKoeficient9MHz = sizeof(s_cvConfigParams9MHz) / sizeof(SClockConfigParams);


static const SClockConfigParams s_cvConfigParams250MHz[] =
{
    {0,0,0,0,0,0,0},         // 0 not use this
    {10,11,2,2,5,550,125},   // 1 finally for 1 bypass should be configured
    {10,11,1,1,11,250,125},  // 2
    {10,7,1,1,7,378,126},    // 3
    {10,5,1,1,5,500,125},   // 4
    {10,4,1,1,5,500,125}   // 5
};

static const int s_cnMaxMultKoeficient250MHz = sizeof(s_cvConfigParams250MHz) / sizeof(SClockConfigParams);


#define SHIFT_FOR_N2_HS 4
#define SHIFT_FOR_N2_LS 1
#define SHIFT_FOR_N1_HS 4
#define SHIFT_FOR_N31   1

#endif // SI5326_CONFIGURE_HEADER_H
