/*****************************************************************************
 * File			: pitz_adc_data_serializer.hpp 
 *                (depricated, should be used 'adc_data_serializer.h' instead)
 * Created on	: 2017 Jun 15
 *****************************************************************************
 * Created by	: D.Kalantaryan, Tel:+49(0)33762/77552 kalantar
 * Email		: davit.kalantaryan@desy.de
 * Mail			: DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 *   This is the header file for 
 *   the class 'pitz::adc::data::Serializer'
 *  Fields
 *		1) 4      B [ 0  -3         ]   Serializer version (for backward compatibility)
 *		2) 4      B [ 4  -7         ]   Spectrum Type (float, double,...)
 *		3) 4      B [ 8  -11        ]   Event Number ()
 *		4) 4	  B [ 12 -15		]	Seconds (keeps the seconds of the sampling start time)
 *		5) 4	  B [ 16 -19		]	Microseconds (keeps the microseconds of the sampling start time)
 *		6) 12	  B [ 20 -31		]	Reserved
 *		7) X*Y	  B [ 32 -32+(X*Y)  ]   Spectrum (X-number of elements, Y-element size)
 *
 ****************************************************************************/

#ifndef __pitz_adc_data_serializer_hpp__
#define __pitz_adc_data_serializer_hpp__

#define HEADER_SIZE		32
#include "ttypess.h"

#define VALUE_IN_THE_OFFSET(_a_type,_a_address)	*((_a_type*)(m_pBuffer+(_a_address)))
#define UINT_VALUE_IN_THE_OFFSET(_a_address)	VALUE_IN_THE_OFFSET(uint32_ttt,_a_address)


namespace pitz{ namespace adc{ namespace data{

namespace SpectrumType{enum _Type{FLOAT=0,DOUBLE=1};}

class Serializer
{
public:
	Serializer();
	virtual ~Serializer();

	/* get function (in the subscriber side) */
	uint32_ttt serializerVersion()const;
	uint32_ttt spectrumType()const;
	uint32_ttt eventNumber()const;
	uint32_ttt seconds()const;
	uint32_ttt microSeconds()const;
	template <typename typeElement>
	const typeElement* spectrum()const;

	/* set functions (in the publisher side) */
	void setSerializerVersion(uint32_ttt serializerVersion);
	void setSpectrumType(uint32_ttt spectrumType);
	void setEventNumber(uint32_ttt eventNumber);
	void setSeconds(uint32_ttt seconds);
	void setMicroSeconds(uint32_ttt microSeconds);
	template <typename typeElement>
	typeElement* setSpectrum();

protected:
	uint8_ttt*		m_pBuffer;

};

}}}


#include "pitz_adc_data_serializer.impl.hpp"


#endif  // #ifndef __pitz_adc_data_serializer_hpp__





/////////////////////////////////////////////////
#pragma once

/*****************************************************************************
* Description
*   Header info
*   Fields
*		1) 2      B [ 0  -1         ]   Start maker '$', 'S'
*		1) 4      B [ 2  -4         ]   Serializer version (for backward compatibility)
*		2) 4      B [ 6  -7         ]   Spectrum Type (float = 0, double = 1)
*		3) 4      B [ 10  -13       ]   Spectrum size
*		4) 4      B [ 14 -17        ]   Event Number ()
*		5) 4	  B [ 18 -21		]	Seconds (keeps the seconds of the sampling start time)
*		6) 4	  B [ 22 -25		]	Microseconds (keeps the microseconds of the sampling start time)
*		7) 8	  B [ 26 -31		]	Reserved
*		8) X*Y	  B [ 34 -32+(X*Y)  ]   Spectrum (X-number of elements, Y-element size)
*
****************************************************************************/


//#include <cstdint>
#include <stdint.h>

#if !defined(BIG_ENDIAN_BP) & !defined(LITTLE_ENDIAN_BP)
#error Either BIG_ENDIAN_BP or LITTLE_ENDIAN_BP must be defined
#endif

#if defined(BIG_ENDIAN_BP) & defined(LITTLE_ENDIAN_BP)
#error Both BIG_ENDIAN_BP and LITTLE_ENDIAN_BP are defined
#endif

namespace ADC_SERIALIZER { enum { ARAY_DATA_FLOAT, ARAY_DATA_DOUBLE }; }
typedef void*(*typeSwapFunction)(void*);

class adc_data_serializer
{
protected:

	static const int header_size = 64;
	char* data_buffer;
	typeSwapFunction m_swap_bytes;

	static void* swap_4_byte(void* a_src);
	static void* swap_8_byte(void* a_src);

public:

	/////////////////////////////////////////////////////////////////////////////////
	// contructor ( creates float type )
	adc_data_serializer(int aArraySize = 2048);

	// destructor
	virtual ~adc_data_serializer();

	//////////////////////////////////////////////////////////////////////////////////
	// header set API
	// set serializer version
	//void set_version(int32_t aVersion); // version should be set according to 

	// set spectum type: float -> 0, double -> 1
	void set_spectrum_type(int32_t aSpectrumType);

	// set spectum size
	void set_spectrum_size(int32_t aSpectrumSize);

	// set event number to internal buffer
	void set_event_number(int32_t aEventNumber);

	// set event time seconds
	void set_time_seconds(int32_t aSeconds);

	void set_time_useconds(int32_t aUSeconds);

	//////////////////////////////////////////////////////////////////////////////////
	// header get API
	// get serializer version
	int32_t get_version();

	// get spectrum type
	int32_t get_spectrum_type();

	// get spectrrum size
	int32_t get_spectrum_size();

	// get event number
	int32_t get_event_number();

	// get time seconds
	int32_t get_time_seconds();

	// get time microseconds
	int32_t get_time_useconds();

	//////////////////////////////////////////////////////////////////////////////////
	// spectrum set API
	// set spectrum item
	// problem: if array type is set to double, but tampalte function is constucted with float
	// then data is corruped
	template <typename T> void set_spectum_item(int aIndex, T aValue);

	// set spectrum array
	template <typename T> void set_spectum_array(T* aTArray);

	//////////////////////////////////////////////////////////////////////////////////
	// spectrum get API
	// makes specrum area swaping in neccessary
	template <typename T> void prepare_spectrum();

	// get spectrum item
	// warning: must be called after void adc_data_serializer::prepare_spectrum()
	template <typename T> T get_spectrum_item(int aIndex);

	// get spectrum array
	// warning: must be called after void adc_data_serializer::prepare_spectrum()
	template <typename T> const T* get_spectrum_array();

	// returns data buffer pointer
	// warning: must be called after void adc_data_serializer::prepare_spectrum()
	const char* get_buffer();
};

#include <adc_data_seriaalizer.impl.h>

