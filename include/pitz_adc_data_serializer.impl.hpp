/*****************************************************************************
 * File			: pitz_adc_data_serializer.impl.hpp
 * Created on	: 2017 Jun 15
 *****************************************************************************
 * Created by	: D.Kalantaryan, Tel:+49(0)33762/77552 kalantar
 * Email		: davit.kalantaryan@desy.de
 * Mail			: DESY, Platanenallee 6, 15738 Zeuthen
 *****************************************************************************
 * Description
 *   This is the header file for 
 *   the class 'pitz::adc::data::Serializer'
 *  Fields
 *		1) 4      B [ 0  -3         ]   Serializer version (for backward compatibility)
 *		2) 4      B [ 4  -7         ]   Spectrum Type (float, double,...)
 *		3) 4      B [ 8  -11        ]   Event Number ()
 *		4) 4	  B [ 12 -15		]	Seconds (keeps the seconds of the sampling start time)
 *		5) 4	  B [ 16 -19		]	Microseconds (keeps the microseconds of the sampling start time)
 *		6) 12	  B [ 20 -31		]	Reserved
 *		7) X*Y	  B [ 32 -32+(X*Y)  ]   Spectrum (X-number of elements, Y-element size)
 *
 ****************************************************************************/
#ifndef __pitz_adc_data_serializer_impl_hpp__
#define __pitz_adc_data_serializer_impl_hpp__

#ifndef __pitz_adc_data_serializer_hpp__
#error this file could not be included directly
#endif

template <typename typeElement>
const typeElement* pitz::adc::data::Serializer::spectrum()const
{
	return (const typeElement*)(m_pBuffer + HEADER_SIZE);
}


template <typename typeElement>
typeElement* pitz::adc::data::Serializer::setSpectrum()
{
	return (typeElement*)(m_pBuffer + HEADER_SIZE);
}


#endif  // #ifndef __pitz_adc_data_serializer_impl_hpp__
