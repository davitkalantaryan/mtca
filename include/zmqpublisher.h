#pragma once
#include "zmqbasesocket.h"

class ZmqPublisher : public BaseZmqSocket
{
public:
	ZmqPublisher(int slot_num, int channel_num); // throws const char*
	~ZmqPublisher();
	
	int send_data(const void* aData, int aSize); // throws const char*
};

