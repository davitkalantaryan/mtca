#pragma once
#include "zmqbasesocket.h"

class ZmqSubscriber : public BaseZmqSocket
{
protected:
	int mn_timeout;
public:
	ZmqSubscriber(const char* aHostName, int slot_num, int channel_num, int aTimeout /*millisecond*/); // throws const char*
	~ZmqSubscriber();
	
	int recv_data(void* aData, int aSize); // throws const char*

};

