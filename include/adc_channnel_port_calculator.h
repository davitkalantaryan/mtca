/*
 *	File: adc_channnel_port_calculator.h
 *
 *	Created on: Mar 17, 2016
 *	Authors:
 *      Davit Kalantaryan (Email: davit.kalantaryan@desy.de)
 *
 *
 */
#ifndef ADC_CHANNNEL_PORT_CALCULATOR_H
#define ADC_CHANNNEL_PORT_CALCULATOR_H

#define     FIRST_PORT_NUMBER       8030
#define     MAX_NUMBER_OF_SLOTS     32
#define     MAX_NUMBER_OF_CHANNELS  32


static inline int adc_channnel_port_calculator(int a_slot_number,int a_channel_number)
{
    int nRet = FIRST_PORT_NUMBER + (MAX_NUMBER_OF_CHANNELS*a_slot_number) + a_channel_number;
    return nRet;
}


#define     FIRST_PORT_NUMBER_TEST       10030
static inline int adc_channnel_port_calculator_test(int a_slot_number,int a_channel_number)
{
    int nRet = FIRST_PORT_NUMBER_TEST + (MAX_NUMBER_OF_CHANNELS*a_slot_number) + a_channel_number;
    return nRet;
}

#endif // ADC_CHANNNEL_PORT_CALCULATOR_H
