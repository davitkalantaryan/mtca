# File sis8300_initialize.pro
# File created : 2017 Jun 9
# Created by : Davit Kalantaryan (davit.kalantaryan@desy.de)
# This file can be used to produce Makefile for daqadcreceiver application
# for PITZ

include(../../common/common_qt/sys_common.pri)

#TEMPLATE = lib
#QMAKE_EXTRA_TARGETS += copy_mex_file
#copy_mex_file.commands = "cp "
#POST_TARGETDEPS += copy_mex_file

INCLUDEPATH += ../../../include

QT -= core
QT -= gui

SOURCES += \
    ../../../src/test/main_sis8300_initialize.cpp \
    ../../../src/lib/sis8300.cpp \
    ../../../src/lib/mtcagen_local.cpp \
    ../../../src/lib/mtca_upciedevlocal.cpp \
    ../../../src/common/common_argument_parser.cpp

HEADERS += \
    ../../../include/stdafx.h \
    ../../../include/sis8300.h \
    ../../../include/si5326_configure_header.h \
    ../../../include/mtcagen_local.h \
    ../../../include/mtca_upciedevlocal.hpp \
    ../../../include/mtca_regbase.hpp \
    ../../../include/mtca_base.h \
    ../../../include/lightweight_mutex.h
