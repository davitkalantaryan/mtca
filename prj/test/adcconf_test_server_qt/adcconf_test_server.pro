
CONFIG += c++11
include (../../common/common_qt/doocs_server_common.pri)

INCLUDEPATH += ../../../include

HEADERS += \
    ../../../src/test/test_adc_conf_fct.hpp

SOURCES += \
    ../../../src/test/test_global_doocs_funcs.cpp \
    ../../../src/test/test_adc_conf_fct.cpp \
    ../../../src/lib/sis8300.cpp \
    ../../../src/lib/mtcagen_local.cpp \
    ../../../src/lib/mtca_upciedevlocal.cpp

