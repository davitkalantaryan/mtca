# File sis8300_initialize.pro
# File created : 2017 Jun 9
# Created by : Davit Kalantaryan (davit.kalantaryan@desy.de)
# This file can be used to produce Makefile for daqadcreceiver application
# for PITZ

include(../../common/common_qt/sys_common.pri)

#TEMPLATE = lib
#QMAKE_EXTRA_TARGETS += copy_mex_file
#copy_mex_file.commands = "cp "
#POST_TARGETDEPS += copy_mex_file

INCLUDEPATH += ../../../include

QT -= core
QT -= gui

SOURCES += \
    ../../../src/test/main_reg_mapper_test.cpp \
    ../../../src/lib/mtca_devregmapper.cpp \
    ../../../src/common/common_argument_parser.cpp

HEADERS += \
    ../../../include/stdafx.h \
    ../../../include/mtca_devregmapper.hpp
