
CONFIG += c++11
include (../../common/common_qt/doocs_server_common.pri)

#INCLUDEPATH += ../../../include/driver
INCLUDEPATH += ../../../include

SOURCES += \
    ../../../src/server/adcconf/global_doocs_funcs.cpp \
    ../../../src/server/adcconf/adc_conf_fct.cpp \
    ../../../src/lib/sis8300.cpp \
    ../../../src/lib/mtcagen_local.cpp \
    ../../../src/lib/mtca_upciedevlocal.cpp \
    ../../../src/server/adcconf/d_clockdivider.cpp

HEADERS += \
    ../../../src/server/adcconf/adc_conf_fct.h \
    ../../../include/driver/sis8300_reg.h \
    ../../../include/driver/sis8300_defs.h \
    ../../../include/sis8300.h \
    ../../../src/server/adcconf/d_clockdivider.h

target.path = /export/doocs/server/$${TARGET}
INSTALLS += target
