
CONFIG += c++11
include (../../common/common_qt/doocs_server_common.pri)

#INCLUDEPATH += ../../../include/driver
INCLUDEPATH += ../../../include

SOURCES += \
    ../../../src/server/sladc_server/sladc_rpc_server.cc \
    ../../../src/lib/mtca_devregmapper.cpp

HEADERS += \
    ../../../src/server/sladc_server/eq_sladc.h \
    ../../../include/mtca_devregmapper.hpp

target.path = /export/doocs/server/$${TARGET}
INSTALLS += target

OTHER_FILES += \
    ../../../doc/comments.udoc
