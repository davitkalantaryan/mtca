# File pcielinkerrorinvestigation.pro
# File created : 25 May 2017
# Created by : Davit Kalantaryan (davit.kalantaryan@desy.de)
# This file can be used to produce Makefile for daqadcreceiver application
# for PITZ


include(../../common/common_qt/sys_common.pri)
TEMPLATE = app
INCLUDEPATH += ../../../include
INCLUDEPATH += /doocs/develop/common/include

#QMAKE_CXXFLAGS += -std=c++11
#QMAKE_CXXFLAGS += -std=c++0x

SOURCES += \
    ../../../src/error/main_pcielinkerrorinvestigation.cpp \
    ../../../src/util/mtca_upciedevlocal.cpp

HEADERS += \
    ../../../include/mtca_upciedevlocal.hpp \
    ../../../include/mtca_regbase.hpp
