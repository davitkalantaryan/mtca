# File remote_plot_spectrum.pro
# File created : 04 Jul 2017
# Created by : Davit Kalantaryan (davit.kalantaryan@desy.de)
# This file can be used to produce Makefile for daqadcreceiver application
# for PITZ
# CONFIG += TEST
# For making test: '$qmake "CONFIG+=TEST" daqadcreceiver.pro' , then '$make'

include(../../common/common_qt/sys_common.pri)
include(../../common/common_qt/gui_with_mainwnd.pri)

win32:INCLUDEPATH += V:/lib/include/zmq
else : INCLUDEPATH += /doocs/zmq/include

win32: LIBS += -LV:/lib
#else : LIBS += -L/doocs/zmq/lib

LIBS += -lzmq
#DEFINES += BIG_ENDIAN_BP
DEFINES += LITTLE_ENDIAN_BP

SOURCES += \
    ../../../src/ui/main_remote_plot_spectrum.cpp \
    ../../../src/ui/pitz_adc_remoteplotspectrum_mainwindow.cpp \
    ../../../src/ui/pitz_adc_remoteplotspectrum_application.cpp \
    ../../../src/zmq/zmqbasesocket.cpp \
    ../../../src/zmq/zmqsubscriber.cpp \
    ../../../src/ui/pitz_adc_remoteplotspectrum_centralwidget.cpp \
    ../../../src/common/common_ui_qt_graphicwidget.cpp \
    ../../../src/ui/pitz_adc_remoteplotspectrum_connectdlg.cpp \
    ../../../src/common/adc_data_serializer.cpp

HEADERS += \
    ../../../src/ui/pitz_adc_remoteplotspectrum_mainwindow.hpp \
    ../../../src/ui/pitz_adc_remoteplotspectrum_application.hpp \
    ../../../include/zmqsubscriber.h \
    ../../../include/zmqbasesocket.h \
    ../../../src/ui/pitz_adc_remoteplotspectrum_centralwidget.hpp \
    ../../../include/common_ui_qt_graphicwidget.hpp \
    ../../../include/common_ui_qt_graphicwidget.impl.hpp \
    ../../../src/ui/pitz_adc_remoteplotspectrum_connectdlg.hpp

